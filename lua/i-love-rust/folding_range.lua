local lsp = vim.lsp
local manage_error = require'i-love-rust.error'.manage_error
local api = vim.api

local M = {}

M.folds = {}

function M.update_fold_ranges(buffer)
     lsp.buf_request(buffer,'textDocument/foldingRange',
		{textDocument = lsp.util.make_text_document_params()}, function(e,result)
	if e ~= nil then
		manage_error(e)
		return
	end
	M.folds[buffer] = result
  vim.api.nvim_buf_attach(buffer, false, {on_detach = function() M.folds[buffer]=nil end})
  vim.api.nvim_buf_call(buffer,function() vim.cmd("normal zx") end)
	end)
end


function M.fold_level()
  local buffer = api.nvim_get_current_buf()
  local line = vim.v.lnum

	line = line - 1
	local fold_level = 0

	for _,range in ipairs(M.folds[buffer] or {}) do
		if range.startLine <= line and range.endLine >= line then
			fold_level = fold_level + 1
		end
	end

	return fold_level
end

function M.setup_lsp_fold(buffer)
  if not buffer or buffer == 0 then
    buffer = api.nvim_get_current_buf()
  end
  vim.api.nvim_buf_call(buffer, function()
      vim.cmd([[ 
       setlocal foldmethod=expr
       setlocal foldexpr=luaeval(\"require'i-love-rust.folding_range'.fold_level()\")
       ]])
   end)
  M.update_fold_ranges(buffer)
end

return M

