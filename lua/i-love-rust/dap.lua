local M = {}
local ilr_config = require'i-love-rust.config'

local function scheduled_error(err)
    vim.schedule(function() vim.notify(err, vim.log.levels.ERROR) end)
end

function M.setup_adapter()
    local dap = require('dap')
    dap.adapters.rt_lldb = ilr_config.options.tools.dap_adapter
    if vim.fn.executable(ilr_config.options.tools.dap_adapter.command) == 0 then
        scheduled_error(ilr_config.options.tools.dap_adapter.command .." not found. Please install it (lldb).")
        return
    end

end

local function get_cargo_args_from_runnables_args(runnable_args)

    local cmd_args = {}

    for _, value in ipairs(runnable_args.cargoArgs) do
        table.insert(cmd_args, value)
    end

    if cmd_args[1] == "run" then
        cmd_args[1]="build"
    end

    table.insert(cmd_args, '--message-format=json')

    if not vim.tbl_isempty(runnable_args.cargoExtraArgs) then
         for _, value in ipairs(runnable_args.cargoExtraArgs) do
             table.insert(cmd_args, value)
         end
     end

    if cmd_args[1] == "test" then
             table.insert(cmd_args, "--")
             table.insert(cmd_args, "IExpectNoTestHasThisName")
    end

    return cmd_args
end

function M.start(args, opt_exec_args)
    if not pcall(require, 'dap') then
        scheduled_error("nvim-dap not found.")
        return
    end

    if not pcall(require, 'plenary.job') then
        scheduled_error("plenary not found.")
        return
    end

    local Job = require('plenary.job')

    local cargo_args = get_cargo_args_from_runnables_args(args)

    local exec_args = args.executableArgs;

    if opt_exec_args ~= nil then
        exec_args = opt_exec_args
    end

    local dap = require'dap'

    vim.notify(
        "Compiling a debug build for debugging. This might take some time...")
    Job:new({
        command = "cargo",
        args = cargo_args,
        cwd = args.workspaceRoot,
        on_exit = function(j, code)
            if code and code > 0 then
                scheduled_error(
                    "An error occured while compiling. Please fix all compilation issues and try again.")
            end
            vim.schedule(function()
                local result = j:result()
                for i = #result,1,-1 do
                    local value = result[i]
                    local status,json = pcall(vim.fn.json_decode,value)
                    if status and type(json) == "table" and json.executable ~= vim.NIL and
                        json.executable ~= nil then
                        local config = {
                            name = "Rust tools debug",
                            type = "rt_lldb",
                            request = "launch",
                            program = json.executable,
                            args = exec_args,
                            cwd = args.workspaceRoot,

                            -- if you change `runInTerminal` to true, you might need to change the yama/ptrace_scope setting:
                            --
                            --    echo 0 | sudo tee /proc/sys/kernel/yama/ptrace_scope
                            --
                            -- Otherwise you might get the following error:
                            --
                            --    Error on launch: Failed to attach to the target process
                            --
                            -- But you should be aware of the implications:
                            -- https://www.kernel.org/doc/html/latest/admin-guide/LSM/Yama.html
                            runInTerminal = false
                        }
                        dap.run(config)
                        break
                    end
                end
            end)
        end
    }):start() --]]
end

return M
