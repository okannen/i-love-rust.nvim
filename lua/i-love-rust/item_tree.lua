-- ?? helps with all the warnings spam
local vim = vim
local util = vim.lsp.util
local config = require('i-love-rust.config')
local manage_error = require'i-love-rust.error'.manage_error

local M = {}

local function get_params()
    return {textDocument = vim.lsp.util.make_text_document_params() }
end

M._state = {winnr = nil, commands = nil}
local set_keymap_opt = {noremap = true}

function M._close_hover()
    if M._state.winnr ~= nil then
        vim.api.nvim_win_close(M._state.winnr, true)
    end
end

function M.handler(e, result)

    if e then
	    manage_error(e)
	    return
    end

    if not result then
        vim.notify('No item')
        return
    end

    local markdown_lines = util.convert_input_to_markdown_lines(result)
    markdown_lines = util.trim_empty_lines(markdown_lines)

    if vim.tbl_isempty(markdown_lines) then
        vim.notify('No item')
        return
    end


    local bufnr, winnr = util.open_floating_preview(markdown_lines, "rust",
                                                    {
        border = config.options.tools.hover_actions.border,
        focusable = true,
        focus_id = "i-love-rust-hover-actions",
        close_events = {"CursorMoved", "BufHidden", "InsertCharPre"}
    })

    if config.options.tools.hover_actions.auto_focus then
        vim.api.nvim_set_current_win(winnr)
    end

    if M._state.winnr ~= nil then return end

    -- update the window number here so that we can map escape to close even
    -- when there are no actions, update the rest of the state later
    M._state.winnr = winnr
    vim.api.nvim_buf_set_keymap(bufnr, "n", "<Esc>",
                                ":lua require'i-love-rust.hover_actions'._close_hover()<CR>",
                                set_keymap_opt)

    vim.api.nvim_buf_attach(bufnr, false,
                            {on_detach = function() M._state.winnr = nil end})

end

-- Sends the request to rust-analyzer to get hover actions and handle it
function M.hover_actions()
    vim.lsp.buf_request(0, "rust-analyzer/viewItemTree", get_params(), M.handler)
end

return M
