-- ?? helps with all the warnings spam
local vim = vim
local rutils = require('i-love-rust.utils')
local ui = require'i-love-rust.ui'
local actions = require'i-love-rust.actions'
local manage_error = require'i-love-rust.error'.manage_error

local M = {}

local function get_params()
    return {
        textDocument = vim.lsp.util.make_text_document_params(),
        position = nil -- get em all
    }
end

local function get_position_params()
    return vim.lsp.util.make_position_params()
end

local function getOptions(result)
    local opts = {}
    for i, runnable in ipairs(result) do
        local str = string.format("%d: %s", i, runnable.label)
	if runnable.location then
        	table.insert(opts, rutils.location_link_to_item(runnable.location, str))
	else
        	table.insert(opts,{text=str})
	end
    end

    return opts
end

local function handler(e, results)
    	if e then
    	        manage_error(e)
    	        return
    	end
        local choices = getOptions(results)

	ui.telescope_window("Runnables", choices, function(choice)
		actions.run_cargo(results[choice])
	   end)
end

-- Sends the request to rust-analyzer to get the runnables and handles them
-- The opts provided here are forwarded to telescope, other than use_telescope
-- which is used to check whether we want to use telescope or the vanilla vim
-- way for input
function M.runnables()
    -- fallback to the vanilla method incase telescope is not installed or the
    -- user doesn't want to use it
    vim.lsp.buf_request(0, "experimental/runnables", get_params(), handler)
end

function M.related_tests()

    -- fallback to the vanilla method incase telescope is not installed or the
    -- user doesn't want to use it
    vim.lsp.buf_request(0, "rust-analyzer/relatedTests", get_position_params(), function(e, result)
            assert(not e,vim.inspect(e))
            local runs = {};
            for _,test in pairs(result) do
                table.insert(runs, test.runnable)
            end
            handler(nil, runs)
        end
    )
end

return M
