local util = require('vim.lsp.util')
local api = vim.api
local M = {}
local to_position = require('i-love-rust.utils').to_position
local actions = require'i-love-rust.actions'
local rutils = require'i-love-rust.utils'

--- bufnr → true|nil
--- to throttle refreshes to at most one at a time
local active_refreshes = {}

--- bufnr -> client_id -> lenses
local lens_cache_by_buf = setmetatable({}, {
  __index = function(t, b)
    local key = b > 0 and b or api.nvim_get_current_buf()
    return rawget(t, key)
  end
})

local namespaces = setmetatable({}, {
  __index = function(t, key)
    local value = api.nvim_create_namespace('rusttools_lsp_codelens:' .. key)
    rawset(t, key, value)
    return value
  end;
})

--@private
M.__namespaces = namespaces

function M.setup_autocmd(buffer)

    local events = "CursorHold, InsertLeave"

    vim.api.nvim_command('augroup CodeLens')
    vim.api.nvim_command(
        'autocmd ' .. events .. ' <buffer='..buffer..'> :lua require"i-love-rust.codelens".refresh('..buffer..')')
    vim.api.nvim_command('augroup END')
end


--@private
local function execute_lens(lens, bufnr, client_id, exec_args)

  local client = vim.lsp.get_client_by_id(client_id)
  assert(client, 'Client is required to execute lens, client_id=' .. client_id)

  actions.commands_to_callbacks({lens.command})[1].callback(exec_args, true)
end


--- Return all lenses for the given buffer
---
---@return table (`CodeLens[]`)
function M.get(bufnr)
  local lenses_by_client = lens_cache_by_buf[bufnr]
  if not lenses_by_client then return {} end
  local lenses = {}
  for _, client_lenses in pairs(lenses_by_client) do
    vim.list_extend(lenses, client_lenses)
  end
  return lenses
end

local function in_range(cursor,range)
  local start = range.start
  local _end = range["end"]
  local line = cursor.line
  local char = cursor.character
  return (line > start.line or (line == start.line and char >= start.character)) and
  (line < _end.line or (line == _end.line and char < _end.character))
end

local function is_nested(sub_range,sup_range)
  return in_range(sub_range.start, sup_range) and in_range(sub_range["end"], sup_range)
end

local function insert_most_nested(options,lens,range, client)
  local inserted = false
  for k,v in pairs(options) do
    if not inserted then
      -- so this lens range is a subrange that the other lens in v
      -- so remove the other lens and keep this one
      if is_nested(range,v.range) then
        options[k] = nil
        table.insert(options,{client=client, lens=lens, range=range})
        inserted=true
        -- so this lens range is a super range of an other one already
        -- selected, do not do any insertion
      elseif is_nested(v.range,range) then
        return
      end
    elseif inserted then
      -- this could happen if their were lens range that intersects without
      -- one being nested within the other. For now this never happens, but the
      -- code is more robust
      if is_nested(range,v.range) then
        options[k] = nil
      end
    end
  end
  -- this lens is not a subrange or super range of an other lens, keep it
  if not inserted then
     table.insert(options,{client=client, lens=lens, range=range})
  end
end

function M.insert_curline_lens_commands(cmd)
  local cursor =api.nvim_win_get_cursor(0)
  local bufnr = api.nvim_get_current_buf()
  local cursor_loc = to_position(cursor,bufnr)
  local lenses_by_client = lens_cache_by_buf[bufnr] or {}
  for _, lenses in pairs(lenses_by_client) do
    for _, lens in pairs(lenses) do
        if cursor_loc.line == lens.range.start.line and in_range(cursor_loc,lens.range) and lens.command then
          lens.command.group = "code lens"
          table.insert(cmd,lens.command)
        end
    end
  end
end

--- Run the code lens in the current line
---
function M.run(cmd,exec_args)
  --M.refresh()
  local cursor =api.nvim_win_get_cursor(0)
  local cursor_loc = {line = cursor[1]-1, character = cursor[2]}
  local bufnr = api.nvim_get_current_buf()
  local options = {}
  local lenses_by_client = lens_cache_by_buf[bufnr] or {}
  for client, lenses in pairs(lenses_by_client) do
    for _, lens in pairs(lenses) do
      local target_range = (lens.command and lens.command.arguments[1].location and 
        lens.command.arguments[1].location.targetRange or nil)
      local lens_cmd = lens.command and lens.command.command or nil
      if cmd == nil or cmd == lens_cmd then
        if in_range(cursor_loc,lens.range) then
          insert_most_nested(options,lens, lens.range, client)
        elseif target_range and in_range(cursor_loc,target_range) then
          insert_most_nested(options,lens, target_range, client)
        end
      end
    end
  end
  if #options == 0 then
    vim.notify('No executable codelens found at current line')
  elseif #options == 1 then
    local option = options[1]
    execute_lens(option.lens, bufnr, option.client, exec_args)
  else
    local options_strings = {"Code lenses:"}
    for i, option in ipairs(options) do
       table.insert(options_strings, string.format('%d. %s', i, option.lens.command.title))
    end
    local choice = vim.fn.inputlist(options_strings)
    if choice < 1 or choice > #options then
      return
    end
    local option = options[choice]
    execute_lens(option.lens, bufnr, option.client, exec_args)
  end
end


--- Display the lenses using virtual text
---
---@param lenses table of lenses to display (`CodeLens[] | null`)
---@param bufnr number
---@param client_id number
function M.display(lenses, bufnr, client_id)
  if not lenses or not next(lenses) then
    return
  end
  local lenses_by_lnum = {}
  for _, lens in pairs(lenses) do
    local line_lenses = lenses_by_lnum[lens.range.start.line]
    if not line_lenses then
      line_lenses = {}
      lenses_by_lnum[lens.range.start.line] = line_lenses
    end
    table.insert(line_lenses, lens)
  end
  local ns = namespaces[client_id]
  local num_lines = api.nvim_buf_line_count(bufnr)
  for i = 0, num_lines do
    local line_lenses = lenses_by_lnum[i]
    api.nvim_buf_clear_namespace(bufnr, ns, i, i + 1)
    local chunks = {}
    for _, lens in pairs(line_lenses or {}) do
      local text = lens.command and lens.command.title or 'Unresolved lens ...'
      if #chunks > 0 then
        table.insert(chunks, {" | ", 'LspCodeLens' })
      end
      table.insert(chunks, {text, 'LspCodeLens' })
    end
    if #chunks > 0 then
      api.nvim_buf_set_virtual_text(bufnr, ns, i, chunks, {})
    end
  end
end

function M.print_lenses()
  print(vim.inspect(lens_cache_by_buf))
end


--- Store lenses for a specific buffer and client
---
---@param lenses table of lenses to store (`CodeLens[] | null`)
---@param bufnr number
---@param client_id number
function M.save(lenses, bufnr, client_id)
  local lenses_by_client = lens_cache_by_buf[bufnr]
  if not lenses_by_client then
    lenses_by_client = {}
    lens_cache_by_buf[bufnr] = lenses_by_client
    local ns = namespaces[client_id]
    api.nvim_buf_attach(bufnr, false, {
      on_detach = function(b) lens_cache_by_buf[b] = nil end,
      on_lines = function(_, b, _, first_lnum, last_lnum)
        api.nvim_buf_clear_namespace(b, ns, first_lnum, last_lnum)
      end
    })
  end
  lenses_by_client[client_id] = lenses
end


--@private
local function resolve_lenses(lenses, bufnr, client_id, callback)
  lenses = lenses or {}
  local num_lens = vim.tbl_count(lenses)
  if num_lens == 0 then
    callback()
    return
  end

  --@private
  local function countdown()
    num_lens = num_lens - 1
    if num_lens == 0 then
      callback()
    end
  end
  local ns = namespaces[client_id]
  local client = vim.lsp.get_client_by_id(client_id)
  for _, lens in pairs(lenses or {}) do
    if lens.command then
      countdown()
    else
      client.request('codeLens/resolve', lens, function(e, result)
        if e ~= nil then
          print("error lens",vim.inspect(e))
          return
        end
        if result and result.command then
          lens.command = result.command
          -- Eager display to have some sort of incremental feedback
          -- Once all lenses got resolved there will be a full redraw for all lenses
          -- So that multiple lens per line are properly displayed
          api.nvim_buf_set_virtual_text(
            bufnr,
            ns,
            lens.range.start.line,
            {{ lens.command.title, 'LspCodeLens' },},
            {}
          )
        end
        countdown()
      end, bufnr)
    end
  end
end


--- |lsp-handler| for the method `textDocument/codeLens`
---
function M.on_codelens(err, result, ctx)
  local client_id = ctx.client_id
  local bufnr = ctx.bufnr
  active_refreshes[bufnr] = true

  if err ~= nil then
    print("error handler", vim.inspect(err))
    return
  end

  M.save(result, bufnr, client_id)

  -- Eager display for any resolved (and unresolved) lenses and refresh them
  -- once resolved.
  M.display(result, bufnr, client_id)
  resolve_lenses(result, bufnr, client_id, function()
    M.display(result, bufnr, client_id)
  end)
  active_refreshes[bufnr] = nil
end


--- Refresh the codelens for the current buffer
---
--- It is recommended to trigger this using an autocmd or via keymap.
---
--- <pre>
---   autocmd BufEnter,CursorHold,InsertLeave <buffer> lua vim.lsp.codelens.refresh()
--- </pre>
---
function M.refresh(bufnr, force)
  if bufnr == nil or bufnr == 0 then
  	bufnr = api.nvim_get_current_buf()
  end
  if active_refreshes[bufnr] and not force then
    return
  elseif force then
    active_refreshes[bufnr] = nil
  end
  local params = {
    textDocument = rutils.make_text_document_params(bufnr)
  }
  vim.lsp.buf_request(bufnr, 'textDocument/codeLens', params)
end


return M
