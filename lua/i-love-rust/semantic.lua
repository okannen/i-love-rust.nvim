local lsp = vim.lsp
local api = vim.api
local manage_error = require'i-love-rust.error'.manage_error
local rutils = require'i-love-rust.utils'
local options = require'i-love-rust.config'.options
local simple_highlighter = require"i-love-rust.highlighting".simple_highlighter

local is_highlighting = false

local M = {}
M.highlights = {}

local namespace_id = api.nvim_create_namespace("i-love-rust-hl")
local namespace_tempo_id = api.nvim_create_namespace("i-love-rust-hl-tempo")

local type_ids = {}
local modifier_ids = {}

local function use_utf8_encoding(client)
  return client.offset_encoding == "utf-8"
end

local function get_legend()
	if M.legend then
		return M.legend
	end
	local clients = lsp.get_active_clients()
	for _,client in ipairs(clients) do
	  if client.name == "rust_analyzer" then
		  M.legend = client.server_capabilities.semanticTokensProvider.legend
      break
	  end
	end
  for i,tp in ipairs(M.legend.tokenTypes) do
    type_ids[tp]=i-1
  end
  for i,m in ipairs(M.legend.tokenModifiers) do
    modifier_ids[m]=i-1
  end
  return M.legend
end

function M.current()
	local buffer = api.nvim_get_current_buf()
	if M.highlights[buffer] == nil then
    M.reset_highlight()
	end
	local cur = vim.fn.getpos('.')
	local cur_line = cur[2] - 1
	local cur_ch = cur[3] - 1
	for _, token in ipairs(M.highlights[buffer].data) do
		if cur_line == token.line and cur_ch >= token.ch_start and cur_ch < token.ch_end then
			print(token:type_string(),vim.inspect(vim.list_extend({},token:modifier_string_list())))
	  elseif cur_line < token.line then
      return
    end
	end
end

local function token_for(buffer, namespace)
  local Token = {buffer=buffer, namespace = namespace}
  function Token:highlight(style)
     --api.nvim_buf_add_highlight(buffer, self.namespace, style, self.line, self.ch_start, self.ch_end)
     api.nvim_buf_set_extmark(buffer,self.namespace,self.line,self.ch_start,
      {
      end_col = self.ch_end,
      hl_group = style,
      })
     --vim.highlight.range(buffer, self.namespace, style, {self.line, self.ch_start}, {self.line,self.ch_end})
  end
  function Token:get_text()
    local line = self.line
	  return string.sub(api.nvim_buf_get_lines(self.buffer,line,line+1,false)[1],self.ch_start+1, self.ch_end)
  end
  function Token:into_Token(token)
    setmetatable(token,{__index = self})
    return token
  end
  function Token:is_a(tp)
    return self.type == type_ids[tp]
  end
  function Token:has_modifier(mod)
    local mod_id = modifier_ids[mod]
    return bit.band(self.modifiers, bit.lshift(1,mod_id)) ~= 0
  end
  function Token:type_string()
    return M.legend.tokenTypes[self.type+1]
  end
  function Token:modifier_string_list()
    local res = {}
    for mod,mod_id in pairs(modifier_ids) do
      if bit.band(self.modifiers, bit.lshift(1,mod_id)) ~= 0 then
        table.insert(res,mod)
      end
    end
    return res
  end
  function Token:get_location(uri)
    if not uri then
      uri = vim.uri_from_bufnr(self.buffer)
    end
    return {
      uri = uri,
      range = {
        start = {
          line = self.line,
          character = self.ch_start_utf16,
        },
        ["end"] = {
          line = self.line,
          character = self.ch_start_utf16 + self.length,
        },
      },
      label = self:get_text()
    }
  end
  return Token
end

local current_highlighter = {
  syntax = "OFF",
  highlight = (options.tools.highlight and options.tools.highlight.token_highlighter) or simple_highlighter,
  temporary_highlight_syntax = options.tools.highlight and options.tools.highlight.temporary_highlight_syntax,
}

if options.tools.highlight and options.tools.highlight.overlay_syntax then
  current_highlighter.syntax = "ON"
end


function M.reset_highlight(buffer, highlighter,timeout)
	buffer = buffer or 0
	highlighter = highlighter or current_highlighter
	api.nvim_buf_clear_namespace(buffer,namespace_id,0,-1)
  if highlighter.temporary_highlight_syntax then
    vim.cmd("source "..highlighter.temporary_highlight_syntax)
  end
	M.reset_semantic(buffer,highlighter,timeout)
end

function M.highlight(buffer, highlighter, timeout, sleep_timeout)
	buffer = buffer or 0
	highlighter = highlighter or current_highlighter
	M.delta_semantic(buffer,highlighter, timeout, sleep_timeout)
end

function M.on_notification(_,_,_,client_id)
  local bufs = lsp.get_buffers_by_client_id(client_id)
  for _,buf in ipairs(bufs) do
      M.setup(buf)
  end
  return true
end

local function relative_to_absolute(buffer,data,c_line, c_col, namespace, utf8_encoding)
  local Tok = token_for(buffer, namespace)
	local nelem = #data
	local _ = get_legend()
	local i = 1
	local target_data = {}
  local lines = nil
  if not utf8_encoding then
    lines = api.nvim_buf_get_lines(buffer,0,-1, false)
  end
	while i < nelem do
		local delta_line = data[i]
		if delta_line == 0 then
			c_col = c_col+data[i+1]
		else
			c_line = c_line + delta_line
			c_col = data[i+1]
		end
		local length = data[i+2]
		local token_type = data[i+3]
		local token_mods = data[i+4]

    local start_ch = c_col+1
    local end_ch = c_col+length
    if not utf8_encoding then
      local curl = lines[c_line+1]
      local ok,start_ch_c = pcall(vim.str_byteindex,curl, c_col+1)
      if not ok and curl then
        start_ch = curl:len()
      else
        start_ch = start_ch_c
      end
      local ok,end_ch_c = pcall(vim.str_byteindex,curl, c_col+length)
      if not ok then
        end_ch = curl:len()
      else
        end_ch = end_ch_c
      end
    end
		table.insert(target_data,
			Tok:into_Token{type = token_type,
			modifiers = token_mods,
			delta_ch = data[i+1],
			delta_line = delta_line,
			length = length,
			line = c_line,
			ch_start = start_ch-1,
			ch_start_utf16 = c_col,
      ch_end = end_ch,
		        })
		i= i + 5
	end
	return target_data
end

local function update_absolute(buffer,data,from,utf8_encoding)

	local nelem = #data

  local c_col = 0
  local c_line = 0
  if from > 1 then
	  c_col = data[from-1].ch_start_utf16
	  c_line = data[from-1].line
  end

	for i = from,nelem do
		local cd = data[i]
		local delta_line = cd.delta_line
		if delta_line == 0 then
			c_col = c_col+cd.delta_ch
			local length = cd.length
      if utf8_encoding then
        cd.ch_start = c_col
        cd.ch_end = c_col+length
      else
			  local start = rutils.from_position({line = c_line, character = c_col},buffer)
			  local ed = rutils.from_position({line = c_line, character = c_col+length}, buffer)
			  cd.ch_start=start[2]-1
			  cd.ch_end=ed[2]-1
      end
			cd.ch_start_utf16 = c_col
			cd.line = c_line
		else
			c_line = c_line + delta_line
			local dl = c_line - cd.line
			if dl ~= 0 then
				for j = i,nelem do
					data[j].line = data[j].line+dl
				end
			end
			return
		end
	end
end

local function do_reset(buffer,res,highlighter,utf8_encoding)
  api.nvim_buf_clear_namespace(buffer,namespace_tempo_id,0,-1)
	local target_data = relative_to_absolute(buffer, res.data, 0, 0,namespace_id,utf8_encoding)
	local resd = {id = res.resultId, data = target_data}
	M.highlights[buffer] = resd
  local highlight = highlighter.highlight
	for _,token in ipairs(target_data) do
		highlight(token)
	end
  if highlighter.syntax=="OFF" and highlighter.temporary_highlight_syntax then
    vim.api.nvim_buf_call(buffer, function ()
      vim.cmd("set syn=OFF")
    end)
  end
end

function M.delta_semantic(buffer,highlighter,timeout,sleep_timeout)
  if not timeout then
    timeout = 100
  end
  if not sleep_timeout then
    sleep_timeout = 3*timeout
  end
	if buffer == nil or buffer == 0 then
		buffer = api.nvim_get_current_buf()
	end
	local old_res = M.highlights[buffer]
	if old_res then
    local old_data = old_res.data
    local old_id = old_res.id
    local rust_analyzer = rutils.buf_get_rust_analyzer(buffer)
    if not rust_analyzer then
      return
    end
    print("request sync");
		local res,e = rust_analyzer.request_sync('textDocument/semanticTokens/full/delta',
      {
        textDocument = rutils.make_text_document_params(buffer),
		    previousResultId = old_id},
        timeout,
        buffer)
	  if e=="timeout"  then
      print("request timeout", sleep_timeout);
      vim.defer_fn(function()
          M.highlight(buffer,highlighter,timeout+20,sleep_timeout+1000)
        end,
        sleep_timeout)
	  	return
	  elseif e then
	    return
	  end

    if res.err then
      manage_error(res[1].err)
      return
    end

    res = res.result

    if res.data then
      do_reset(buffer,res,highlighter,use_utf8_encoding(rust_analyzer))
      return
    end


    -- algorithm:
    -- I. Turn relative index into absolute and recreate the
    --    update highlighting data
    -- II. Update highlight over changed lines

    -- I. Update semantic data
    local last_elem = 1
    local new_data = {}
    local inserts_ranges = {}

    if not res.edits then
      old_data.id = res.resultId
      M.highlights[buffer] = {id=res.resultId, data=old_data}
      return
    end

    for _,edit in ipairs(res.edits or {}) do
	    local delete_count = edit.deleteCount/5
	    local start = edit.start/5+1

	    local prev_count = #new_data
	    vim.list_extend(new_data, old_data,last_elem,start-1)
	    update_absolute(buffer,new_data,prev_count+1,use_utf8_encoding(rust_analyzer))

	    local c_line = 0
	    local c_col = 0

	    local prev_i = #new_data
	    if prev_i ~= 0 then
	    	local prev = new_data[prev_i]
	    	c_line = prev.line
	    	c_col = prev.ch_start_utf16
	    end

      if edit.data and #edit.data > 0 then
	      local insert_data = relative_to_absolute(buffer, edit.data, c_line, c_col,namespace_id
          ,use_utf8_encoding(rust_analyzer))

        local ld = #new_data + 1
        table.insert(inserts_ranges, {start = ld, _end = ld + #insert_data - 1})

	      vim.list_extend(new_data,insert_data)
      end

	    last_elem = start+delete_count
   end

   local prev_count = #new_data
   vim.list_extend(new_data, old_data, last_elem)
   update_absolute(buffer,new_data,prev_count+1,use_utf8_encoding(rust_analyzer))

   -- II. update highlighting:
   --   1. Merge inserts that are line contiguous
   --   2. clear namespace over the lines of the merged insert
   --   3. highlight the merged insert
   local highlight = highlighter.highlight

   local i = 1
   local l = #inserts_ranges
   local ld = #new_data

   while i <= l do
     local ins = inserts_ranges[i]
     local start_ind = ins.start
     local end_ind = ins._end
     local line_start = new_data[start_ind].line
     local line_end = new_data[end_ind].line

     --   1. merge line contiguous inserts
     i = i+1
     while i<=l do
       ins = inserts_ranges[i]
       if new_data[ins.start].line >= line_end + 1 then
         end_ind = ins._end
         line_end = new_data[end_ind].line
       end
     end

     --   2. clear namespace
     api.nvim_buf_clear_namespace(buffer,namespace_tempo_id,0,-1)
     vim.api.nvim_buf_clear_namespace(buffer,namespace_id, line_start, line_end + 1)

     --   3. apply highlight
     --
     --     find first token on start line
     start_ind = start_ind - 1
     while start_ind>1 and new_data[start_ind].line == line_start do
       start_ind = start_ind - 1
     end
     start_ind = start_ind + 1

     --     find last token on last line
     end_ind = end_ind + 1
     while end_ind<=ld and new_data[end_ind].line == line_end do
       end_ind = end_ind + 1
     end
     end_ind = end_ind - 1

     --     apply highlight
     for j = start_ind,end_ind do
        	highlight(new_data[j])
     end

   end

   M.highlights[buffer] = {id = res.resultId, data = new_data}
	else
		M.reset_semantic(buffer,highlighter,timeout, sleep_timeout)
	end
end

function M.redraw()
     --   2. clear namespace
     api.nvim_buf_clear_namespace(0,namespace_tempo_id,0,-1)
     api.nvim_buf_clear_namespace(0,namespace_id, 0, -1)
     local buffer = api.nvim_get_current_buf()
     local data = M.highlights[buffer].data
     local highlight = current_highlighter.highlight
     for _,tok in ipairs(data) do
        highlight(tok)
     end
end

function M.reset_semantic(buffer,highlighter,timeout,sleep_timeout)
  if not timeout then
    timeout = 100
  end
  if not sleep_timeout then
    sleep_timeout = 3*timeout
  end
	if buffer == nil or buffer == 0 then
		buffer = api.nvim_get_current_buf()
	end
  local rust_analyzer = rutils.buf_get_rust_analyzer(buffer)
  if not rust_analyzer then
    return
  end
  --local server_statuses = require'i-love-rust.server_status'.server_statuses()
  --local status = server_statuses[rust_analyzer.id]
  --if not status or not status.quiescent then
  --  return
  --end
	local res,e = rust_analyzer.request_sync(
    'textDocument/semanticTokens/full',
	  {textDocument = rutils.make_text_document_params(buffer)},
    timeout,
    buffer)
	if e=="timeout" then
    vim.defer_fn(function()
        M.highlight(buffer,highlighter,timeout+10,sleep_timeout+1000)
      end,
      sleep_timeout)
		return
	elseif e then
	  return
	end
  -- TODO: only rust-analyzer client
  if res.err then
    manage_error(res.err)
    return
  end
  do_reset(buffer,res.result,highlighter,use_utf8_encoding(rust_analyzer))
end

function M.range_semantic(buffer,range,highlighter)
  buffer = buffer or 0
	lsp.buf_request(buffer,'textDocument/semanticTokens/range',
		{
			textDocument = rutils.make_text_document_params(buffer),
			range = range,
		},
		function(e,res, ctx)
      local client_id = ctx.client_id
      local bufnr = ctx.bufnr
			if e ~= nil then
				manage_error(e)
				return
			end
      local client = lsp.get_client_by_id(client_id)
			local target_data = relative_to_absolute(bufnr, res.data, 0, 0,namespace_tempo_id,
        use_utf8_encoding(client))
      local highlight = highlighter.highlight
      api.nvim_buf_clear_namespace(buffer, namespace_tempo_id,range.start.line+1,range["end"].line)
			for _,token in ipairs(target_data) do
				highlight(token)
			end
		end
		)
end
function M.with_buffer_semantic(buffer,f)
  buffer = buffer or 0
	lsp.buf_request(buffer,'textDocument/semanticTokens/full',
		{
			textDocument = rutils.make_text_document_params(buffer),
		},
		function(e,res,ctx)
      local client_id = ctx.client_id
      local bufnr = ctx.bufnr
			if e ~= nil then
				manage_error(e)
				return
			end
      local client = lsp.get_client_by_id(client_id)
			local target_data = relative_to_absolute(bufnr, res.data, 0, 0,namespace_tempo_id, use_utf8_encoding(client))
      f(target_data)
		end
		)
end

function M.currentline_highlight(highlighter)
	local cl = vim.fn.line('.')
	local range = {
		start = {
			line = cl-1,
			character = 0
		},
	        ["end"] = {
			line = cl,
			character = 0
		}
		}
	local buffer = api.nvim_get_current_buf()
	highlighter = highlighter or current_highlighter
	M.range_semantic(buffer,range,highlighter)
end

function M.highlight_window(highlighter)
	local buffer = api.nvim_get_current_buf()
  if rutils.callback_is_clean(buffer,"highlighting") then
    return
  end
	local s = vim.fn.line('w0')
	local e = vim.fn.line('w$')
  local nl = api.nvim_buf_line_count(0)
  if s < 10 then
    s = 0
  else
    s = s - 10
  end
  if e+10 > nl then
    e = nl
  else
    e = e + 10
  end
	local range = {
		start = {
			line = s,
			character = 0
		},
	        ["end"] = {
			line = e,
			character = 0
		}
		}
	highlighter = highlighter or current_highlighter
	M.range_semantic(buffer,range,highlighter)
end

local function enable_highlighting(buffer)
  rutils.register_txt_change_callback(buffer,"highlighting",function()
    M.highlight(buffer)
  end)
  vim.api.nvim_buf_call(buffer,
    function()
      vim.cmd("set syn="..current_highlighter.syntax)
      if current_highlighter.highlight then
        vim.cmd("augroup i_love_rust_semantic")
        vim.cmd("autocmd! * <buffer>")
        --vim.cmd("autocmd BufWrite,CursorHold <buffer> lua require'i-love-rust.semantic'.highlight()")
        vim.cmd("autocmd WinScrolled,WinEnter,TextChangedI,TextChanged <buffer> lua require'i-love-rust.semantic'.highlight_window()")
        vim.cmd("augroup END")
        M.reset_highlight(buffer)
      else
        rutils.remove_txt_change_callback(buffer,"highlighting")
        vim.cmd([[
        augroup i_love_rust_semantic
          au! * <buffer>
        augroup END
        ]])
	      api.nvim_buf_clear_namespace(buffer,namespace_id,0,-1)
	      api.nvim_buf_clear_namespace(buffer,namespace_tempo_id,0,-1)
		    M.highlights[buffer] = nil
      end
    end)
end

function M.enable_highlighting()
  is_highlighting = true
  rutils.for_all_rust_buffers(enable_highlighting)
end

function M.set_highlighter(highlight, overlaying_syntax, temporary_highlight_syntax)
  local syntax = "OFF"
  if overlaying_syntax then
    syntax = "ON"
  end
  current_highlighter = {
    highlight = highlight,
    syntax = syntax,
    temporary_highlight_syntax = temporary_highlight_syntax
  }
  if is_highlighting then
    M.enable_highlighting()
  else
    rutils.for_all_rust_buffers(
      function(buffer)
	      api.nvim_buf_clear_namespace(buffer,namespace_id,0,-1)
	      api.nvim_buf_clear_namespace(buffer,namespace_tempo_id,0,-1)
	  	  M.highlights[buffer] = nil
      end
    )
  end
end

local function disable_highlighting(buffer)
  rutils.remove_txt_change_callback(buffer,"highlighting")
  vim.api.nvim_buf_call(buffer, function()
    vim.cmd(string.format([[
    set syn=ON
    augroup i_love_rust_semantic
      au! * <buffer=%d>
    augroup END
    ]],buffer,buffer))
	  api.nvim_buf_clear_namespace(buffer,namespace_id,0,-1)
	  api.nvim_buf_clear_namespace(buffer,namespace_tempo_id,0,-1)
		M.highlights[buffer] = nil
  end)
end

function M.disable_highlighting()
  is_highlighting = false
  rutils.for_all_rust_buffers(disable_highlighting)
end

M.is_clean = {}

function M.setup(buffer)
  vim.schedule(function()
    if options.tools.highlight then
      if buffer then
        enable_highlighting(buffer)
      else
        rutils.for_all_rust_buffers(enable_highlighting)
      end
    end
  end)
end

return M

