local M = {}
-- ?? helps with all the warnings spam
local vim = vim

local manage_error = require'i-love-rust.error'.manage_error

-- Sends the request to rust-analyzer to get the inlay hints and handle them
function M.crate_graph(full)
    local full_cond = full or false
    vim.lsp.buf_request(0, "rust-analyzer/viewCrateGraph", {full = full_cond},
    function(err, result)

	    if err then
	    	manage_error(err)
	    	return
	    end

      local file_name = os.tmpname();
      local file = io.open(file_name,"w")
      file:write(result)
      file:close()
      vim.loop.spawn( vim.g.i_love_rust_open_cmd,
          {args = {file_name},
              stdio = {nil,nil,nil}
          })
    end
)
end

return M
