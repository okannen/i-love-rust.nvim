-- ?? helps with all the warnings spam
local vim = vim
local util = vim.lsp.util
local api = vim.api
local config = require'i-love-rust.config'
local lsp = vim.lsp
local uv = vim.loop
local rutils = require'i-love-rust.utils'

local M = {}

local context_windows = {}

local function get_window(id)
  local by_tabs = context_windows[api.nvim_get_current_tabpage()]
  return by_tabs and by_tabs[id]
end

local function set_window(id,win)
  local tabh = api.nvim_get_current_tabpage()
  local by_tabs = context_windows[tabh]
  if by_tabs then
    by_tabs[id] = win
  else
    context_windows[tabh]={[id]=win}
  end
end
local function get_all_windows(id)
  local wins = {}
  for _,by_tab in ipairs(context_windows) do
    table.insert(wins, by_tab[id])
  end
  return wins
end


-- run command under the cursor
function M.run_command(id, ind)

    local win = get_window(id)

    if not win then
       print("window id",vim.inspect(id))
       vim.notify(string.format("no context window with id %s", id),vim.log.levels.ERROR)
       return
    end

    local action = win.commands[ind]

    if win.is_floating  then
        M.close_window(id)
    else
        vim.api.nvim_set_current_win(get_window(id).associated_win)
    end

    if action == nil then
        vim.notify(string.format("no command n°%d", ind),vim.log.levels.ERROR)
    else
        action()
    end
end

function M.run_command_under_cursor(id)

   local win = get_window(id)

   if not win then
       print("window id",vim.inspect(id))
       vim.notify("no context window with this id",vim.log.levels.ERROR)
       return
   end

   local line = vim.api.nvim_win_get_cursor(win.winnr)[1]

   local ind = win.line_command_map[line]

   M.run_command(id, ind)
end


function M.close_window(id)
    for _,win in ipairs(get_all_windows(id)) do
      if win ~= nil then
          local winnr = win.winnr
          set_window(id, nil)
          api.nvim_win_close(winnr, true)
      end
    end
end

function M.exist(id)
    return get_window(id) ~= nil
end

function M.focus(id)
    if M.exist(id) then
        vim.api.nvim_set_current_win(get_window(id).winnr)
    end
end

local function get_group(command)
    if command.group then
        return command.group
    elseif command.kind == "quickfix" then
        return "quickfix"
    else
        return "Miscellaneous commands"
    end
end

local function group_order(a,b)
    -- quickfix on top and code lens on bottom
    if a == b then
        return false
    elseif a == "quickfix" then
        return true
    elseif b == "quickfix" then
        return false
    elseif a == "code lens" then
        return false
    elseif b == "code lens" then
        return true
    else
        return a > b
    end
end


local function apply_commands(text_lines,commands)
    local cl = nil
    local res_com = {}
    local line_command_map = {}
    if not vim.tbl_isempty(commands) then
        local act_txt = {}

        cl = nil

        local grouped = {}
        local cind = 1

        local prefered_group = nil

        for _,command in ipairs(commands) do
            local group = get_group(command)
            if not grouped[group] then
                grouped[group] = {command}
            else
                if command.isPreferred then
                    local nt = {command}
                    vim.list_extend(nt,grouped[group])
                    grouped[group] = nt
                    prefered_group = group
                else
                    table.insert(grouped[group],command)
                end
            end
        end

        local groups = {}
        for k,_ in pairs(grouped) do
            table.insert(groups,k)
        end

        table.sort(groups, group_order)

        if prefered_group then
              local tg = grouped[prefered_group]
              table.insert(act_txt,"*"..prefered_group.."*")
              for _,command in ipairs(tg) do
                res_com[cind] = command.callback
                if not cl then
                    cl = #act_txt - 1
                end
                table.insert(act_txt,string.format("%d", cind) .. ". " .. command.message)
                line_command_map[#act_txt] = cind
                cind = cind + 1
              end
        end

        for _,group_name in ipairs(groups) do
              if group_name ~= prefered_group then
                table.insert(act_txt,"*"..group_name.."*")
                for _,command in ipairs(grouped[group_name]) do
                  res_com[cind] = command.callback
                  if not cl then
                      cl = #act_txt - 1
                  end
                  table.insert(act_txt,string.format("%d", cind) .. ". " .. command.message)
                  line_command_map[#act_txt] = cind
                  cind = cind + 1
                end
              end
        end

        if #text_lines ~= 0 then
            table.insert(act_txt,"---")
            vim.list_extend(act_txt, text_lines)
        end

        text_lines = act_txt
    end
    return cl,res_com, line_command_map, text_lines
end

function M.clear_window(id)
    M.open_side_window(id, {}, {}, false)
end

function M.open_side_window(id, text_lines, commands, focus, cur_win_id)

    local cl, res_com, line_command_map, text_lines = apply_commands(text_lines, commands)

    local bufnr = vim.api.nvim_create_buf(false,true)

    if not vim.tbl_isempty(text_lines) then
        util.stylize_markdown(bufnr, text_lines, {})
    end

    local winnr = nil

    local win = get_window(id)
    if not win or not vim.api.nvim_win_is_valid(win.winnr) then
        win = {}
        --should be atomic until focus back
        local cur_winnr = api.nvim_get_current_win()
        if cur_win_id and cur_win_id ~= cur_winnr then
          return
        end
        win.associated_win = cur_win_id or cur_winnr
        vim.cmd('30 vsplit')
        winnr = api.nvim_get_current_win()
        win.winnr = winnr
        -- makes more sense in a dropdown-ish ui
        api.nvim_win_set_option(winnr, 'cursorline', true)
        api.nvim_win_set_option(winnr, 'relativenumber', false)
        api.nvim_win_set_option(winnr, 'number', false)
        api.nvim_win_set_option(winnr,"wrap",false)
        api.nvim_win_set_option(winnr,"foldenable",false)
        api.nvim_win_set_option(winnr, 'signcolumn', "no")
        -- move cursor to the start since its at the place before we added the
        -- commands text
        if cl then
            vim.api.nvim_win_set_cursor(winnr, {cl + 1, 0})
        end
        if not focus then
            pcall(vim.api.nvim_set_current_win,cur_winnr)
        end
        set_window(id,win)
    else
        winnr = win.winnr
        win.associated_win = api.nvim_get_current_win()
        if focus then
            vim.api.nvim_set_current_win(win.winnr)
        end
    end

    win.commands = res_com
    win.line_command_map = line_command_map

    vim.api.nvim_win_set_buf(winnr,bufnr)
    --api.nvim_buf_set_option(bufnr,'modifiable', false)
    --api.nvim_buf_set_option(bufnr,'bufhidden', "wipe")

    vim.api.nvim_buf_attach(bufnr, false,
                            {on_detach = function() 
                              print("set nul")
                              set_window(id,nil) 
                            end})

    --- stop here if there are no possible actions
    if win.commands == nil then
        return
    end

    -- run the command under the cursor
    vim.api.nvim_buf_set_keymap(bufnr, "n", "<CR>",
                                ":lua require'i-love-rust.ui'.run_command_under_cursor(\"" .. id .. "\",true)<CR>",{})
end

function M.open_floating_window(id, text_lines, commands, focus)

    if M.exist(id) then
        M.close_window(id)
    end

    local cl, res_com, line_command_map, text_lines = apply_commands(text_lines, commands)

    if vim.tbl_isempty(text_lines) then
        return
    end

    local bufnr, winnr = util.open_floating_preview(text_lines, "markdown",
                                                    {
        border = config.options.tools.hover_actions.border,
        focusable = true,
        focus_id = "i-love-rust-hover-actions",
        close_events = {"CursorMoved", "BufHidden", "InsertCharPre","CursorMovedI"}
    })
    if focus then
        vim.api.nvim_set_current_win(winnr)
    end

    -- update the window number here so that we can map escape to close even
    -- when there are no actions, update the rest of the state later
    set_window(id,{
        commands = res_com,
        line_command_map = line_command_map,
        winnr =winnr,
        associated_win = api.nvim_get_current_win(),
        is_floating = true
    })

    vim.api.nvim_buf_set_keymap(bufnr, "n", "<Esc>",
                                ":lua require'i-love-rust.ui'.close_window(\"" .. id .. "\")<CR>",{})

    vim.api.nvim_buf_attach(bufnr, false,
                            {on_detach = function() set_window(id,nil) end})

    --- stop here if there are no possible actions
    if #res_com == 0 then
        return
    end

    -- makes more sense in a dropdown-ish ui
    vim.api.nvim_win_set_option(winnr, 'cursorline', true)
    -- move cursor to the start since its at the place before we added the
    -- commands text
    vim.api.nvim_win_set_cursor(winnr, {cl+1, 0})
    -- run the command under the cursor
    vim.api.nvim_buf_set_keymap(bufnr, "n", "<CR>",
                                ":lua require'i-love-rust.ui'.run_command_under_cursor(\"" .. id .. "\")<CR>",{})
end

function M.telescope_window(title, choices, select_callback)
    local pickers = require('telescope.pickers')
    local finders = require('telescope.finders')
    local sorters = require('telescope.sorters')
    local actions = require('telescope.actions')
    local make_entry = require('telescope.make_entry')
    local action_state = require('telescope.actions.state')
    local conf = require('telescope.config')

    local attach_mappings = select_callback and function(bufnr, map)
    	local function fill_loclist()
    	    actions.close(bufnr)
    	    util.set_loclist(choices)
	    vim.cmd("lopen")
    	end

        local function on_select()
            local choice = action_state.get_selected_entry().index
	    actions.close(bufnr)
	    select_callback(choice)
        end

        map('n', '<CR>', on_select)
        map('i', '<CR>', on_select)
        map('n','<C-l>', fill_loclist)
        map('i','<C-l>', fill_loclist)

        -- Additional mappings don't push the item to the tagstack.
        return true
    end or function(bufnr, map)
    	local function fill_loclist()
    	    actions.close(bufnr)
    	    util.set_loclist(choices)
	    vim.cmd("lopen")
    	end
        map('n','<C-l>', fill_loclist)
        map('i','<C-l>', fill_loclist)
	return true
    end

    pickers.new({}, {
        prompt_title = title,
        finder = finders.new_table({results = choices,
    	entry_maker = make_entry.gen_from_quickfix({}),
    	}),
        sorter = sorters.get_generic_fuzzy_sorter(),
        previewer = conf.values.qflist_previewer({}),
        attach_mappings = attach_mappings
    }):find()
end
--TODO nvim_win_set_option( ,"winhl","Normal:MyNormal")
local function compute_preview_size(label_count,label_width)
  --cmdheight --laststatus
  local vim_size = {vim.o.lines, vim.o.columns}

  local cursor_pos
  do
    local win_pos = api.nvim_win_get_position(0)
    local cursor_buf_pos = api.nvim_win_get_cursor(0)
    local gpw = vim.fn.getpos("w0")
    local win_buf_pos = {gpw[2], gpw[3]-1}
    cursor_pos = {
      win_pos[1]+cursor_buf_pos[1]-win_buf_pos[1],
      win_pos[2]+cursor_buf_pos[2]-win_buf_pos[2]
    }
  end

  local list_width = math.floor(vim_size[2]/3)
  if list_width > label_width then
    list_width = label_width
  end
  if list_width < 5 then
    list_width = 5
  end

  local preview_width = vim_size[2]-list_width-3
  if preview_width < 20 then
    preview_width = 0
  elseif preview_width > 120 then
    preview_width = 120
  end

  local list_height = label_count
  --if list_height < 30 then
  --  list_height = 30
  --end
  if list_height > vim_size[1]-3 then
    list_height = vim_size[1]-3
  end
  local preview_max_height = vim_size[1]-3

  local line_pos = cursor_pos[1]+1
  if line_pos + list_height > vim_size[1] - 3 then
    line_pos = vim_size[1]-3 - list_height
  end

  local char_pos = cursor_pos[2]+1
  if char_pos + list_width+4+preview_width > vim_size[2] then
    char_pos = vim_size[2] - list_width - 4 -preview_width
  end
  local at_bottom = vim_size[1] - 3 -line_pos
  return line_pos, char_pos, list_width, list_height, preview_max_height, preview_width, at_bottom
end

local preview_window = nil
local list_buffer = nil
local list_window = nil
local preview_pos = nil
local max_preview_height = nil
local min_preview_height = nil
local preview_height_at_bottom = nil
local preview_width = nil
local list_locations = {}
local preview_ns = api.nvim_create_namespace("i_love_rust_preview")
local previewed_buffer = nil

local preview_leave_reenter = false
function M.preview_leave()
  if preview_leave_reenter then
    return
  end
  preview_leave_reenter = true
  if previewed_buffer then
    api.nvim_buf_clear_namespace(previewed_buffer,preview_ns,0,-1)
    previewed_buffer = nil
  end
  if preview_window then
    api.nvim_win_close(preview_window,true)
    preview_window = nil
  end
  if list_window then
    api.nvim_win_close(list_window,true)
    preview_window = nil
  end
  list_window = nil
  list_locations={}
  preview_leave_reenter = false
end

vim.cmd([[
  hi def link CodePreviewNormal Normal
  hi def link ListPreviewNormal Normal
  ]])

function M.preview_update()
  if preview_width == 0 then
    return
  end
  local item = api.nvim_win_get_cursor(list_window)[1]
  local location = list_locations[item]
  if not location then
    return
  end
  local buffer = vim.uri_to_bufnr(location.uri or location.targetUri)

  local start
  local _end

  if location.range then
    start = location.range.start
    _end = location.range["end"]
  else
    start = location.targetRange.start
    _end = location.targetRange["end"]
  end

  if previewed_buffer then
    api.nvim_buf_clear_namespace(previewed_buffer,preview_ns,0,-1)
    previewed_buffer = nil
  end
  previewed_buffer = buffer

  local pos_start = rutils.from_position(start)
  local pos_end = rutils.from_position(_end)
  api.nvim_buf_set_extmark(buffer,preview_ns,start.line,pos_start[2]-1,
   {
   end_col = pos_end[2]-1,
   end_line = _end.line,
   hl_group = "CursorLine",
   hl_eol = true
   })

  local line_count = api.nvim_buf_line_count(buffer)
  local range_line = _end.line-start.line
  if range_line == 0 then
    range_line = 1
  end

  local height = range_line + 10

  if height > line_count then
    height = line_count
  end

  if height > max_preview_height then
    height = max_preview_height
  elseif height < min_preview_height then
    height = min_preview_height
  end

  local line_pos = preview_pos[1]
  if height > preview_height_at_bottom then
    line_pos = line_pos +preview_height_at_bottom - height
  end

  local conf = {
      relative="editor",
      anchor = "NW",
      width = preview_width,
      height = height,
      row = line_pos,
      col = preview_pos[2],
      focusable = false,
      style = "minimal",
      border = "single",
    }
  vim.fn.bufload(buffer)
  if preview_window ~= nil then
    api.nvim_win_set_buf(preview_window, buffer)
    api.nvim_win_set_config(preview_window,conf)
  else
    preview_window = api.nvim_open_win(buffer, false, conf)
  end

  api.nvim_win_set_option(preview_window,"wrap",false)
  api.nvim_win_set_option(preview_window,"foldenable",false)
  api.nvim_win_set_option(preview_window,'cursorline', true)
  api.nvim_win_set_option(preview_window,'signcolumn', "no")
  api.nvim_win_set_option(preview_window,"wrap",false)
  api.nvim_win_set_option(preview_window,"foldenable",false)
  api.nvim_win_set_option(preview_window,"winhl","Normal:CodePreviewNormal")

  local top_line = start.line+1
  if height > range_line then
    top_line = top_line-math.floor((height-range_line)/2)
    if top_line < 1 then
      top_line = 1
    end
  end

  api.nvim_win_call(preview_window, function()
    vim.fn.winrestview({
      topline = top_line
    })
  end)

end

function M.test()
  local pos = util.make_range_params()
  pos.uri = pos.textDocument.uri
  local pos2 = vim.deepcopy(pos)
  pos2.range.start.line = pos2.range.start.line+10
  pos2.range["end"].line = pos2.range["end"].line+15
  M.locations_view({{label="first",location=pos},{label="second",location=pos2}})
end

local function simple_location_to_item(loc)
  local uri = loc.uri or loc.targetUri
  local buffer = vim.uri_to_bufnr(uri)
  local range = loc.range or loc.targetRange
  local start_pos = rutils.from_position(range.start,buffer)
  return {
    bufnr = buffer,
    lnum = start_pos[1],
    col = start_pos[2],
    text = loc.label
  }
end
local function simple_locations_to_items(loc_list)
  local item_list = {}
  for _,l in ipairs(loc_list) do
    table.insert(item_list,simple_location_to_item(l))
  end
  return item_list
end

function M.preview_jump()
  local item = api.nvim_win_get_cursor(list_window)[1]
  local list_l = list_locations
  M.preview_leave()
  util.set_loclist(simple_locations_to_items(list_l))
  if item > 1 then
    item = item - 1
    vim.cmd(item.."lnext")
  else
    vim.cmd(item.."lfirst")
  end
end

local function make_label(loc)
  local is_windows = uv.os_uname().version:match'Windows'
  local path_sep = is_windows and '\\' or '/'
  local uri = loc.uri or loc.targetUri
  local fname = uri:gsub(".*"..path_sep,"")
  local range = loc.range or loc.targetRange
  return fname.."|"..(range.start.line+1).." col "..(range.start.character+1).."|"
end

local function make_list_buf(buf_lines)
  local buf = api.nvim_create_buf(false,true)
  api.nvim_buf_set_lines(buf, 0, -1, true, buf_lines)
  vim.cmd(string.format("au CursorMoved <buffer=%d> lua require'i-love-rust.ui'.preview_update()",buf))
  vim.cmd(string.format("au WinLeave <buffer=%d> lua require'i-love-rust.ui'.preview_leave()",buf))
  api.nvim_buf_set_keymap(buf,"n","<Esc>","<cmd>lua require'i-love-rust.ui'.preview_leave()<CR>",{})
  api.nvim_buf_set_keymap(buf,"n","<CR>","<cmd>lua require'i-love-rust.ui'.preview_jump()<CR>",{})
  api.nvim_buf_set_keymap(buf,"n","dd","<cmd>lua require'i-love-rust.ui'.delete_item()<CR>",{})
  return buf
end

local function list_buf_after_win_setup(buf)
  api.nvim_buf_set_option(buf,'modifiable', false)
  api.nvim_buf_set_option(buf,'bufhidden', "wipe")
end

function M.delete_item()

  local item = api.nvim_win_get_cursor(list_window)[1]
  local item_count = #list_locations

  if item_count == 1 then
    M.preview_leave()
    return
  end

  api.nvim_buf_set_option(list_buffer,'modifiable', true)
  local _end = item
  if _end == item_count then
    _end = -1
  end
  api.nvim_buf_set_lines(list_buffer,item-1,_end,true,{})
  api.nvim_buf_set_option(list_buffer,'modifiable', false)

  if item == 1 then
    list_locations = vim.list_extend({},list_locations,2)
  else
    local nlocs = vim.list_extend({},list_locations,1,item-1)
    if item ~= item_count then
      vim.list_extend(nlocs,list_locations,item+1)
    end
    list_locations = nlocs
  end

  M.preview_update()

end

function M.locations_view(locations)
  if #locations == 0 then
    return
  end
  print("preview")
  local buf_lines = {}
  local label_max_len = 0
  list_locations = {}
  for _,loc in ipairs(locations) do
    local label = loc.label
    if not label then
      label = make_label(loc)
    end
    local l = label:len()
    if l > label_max_len then
      label_max_len = label:len()
    end
    table.insert(buf_lines, label)
    table.insert(list_locations,loc)
  end

  local buf = make_list_buf(buf_lines)

  local line_pos, char_pos, list_width, list_height, preview_max_height, prev_width, preview_at_bot
    = compute_preview_size(#locations,label_max_len)
  preview_pos = {line_pos, char_pos + list_width+1}
  max_preview_height = preview_max_height
  preview_width = prev_width
  preview_height_at_bottom = preview_at_bot
  min_preview_height = list_height

  list_window = api.nvim_open_win(buf,true,{
    relative="editor",
    anchor = "NW",
    width = list_width,
    height = list_height,
    row = line_pos,
    col = char_pos,
    focusable = false,
    style = "minimal",
    border = "single",
  })
  api.nvim_win_set_option(list_window, 'cursorline', true)
  api.nvim_win_set_option(list_window, 'signcolumn', "no")
  api.nvim_win_set_option(list_window,"wrap",false)
  api.nvim_win_set_option(list_window,"foldenable",false)
  api.nvim_win_set_option(list_window,"winhl","Normal:ListPreviewNormal")
  list_buf_after_win_setup(buf)
end


return M
