local inlay = require('i-love-rust.inlay_hints')
local codelens = require('i-love-rust.codelens')
local semantic = require('i-love-rust.semantic')

local lsp = vim.lsp
local api = vim.api

local M = {}
local manage_error = require'i-love-rust.error'.manage_error

local call_back = nil

local last_result = {}

function M.on_notification(err, result, ctx)
  local client_id = ctx.client_id
  if err then
	  manage_error(err)
	  return
  end

  if client_id == nil then
    return
  end

  last_result[client_id] = result
  if result.quiescent then
    vim.schedule(function()
        local bufs = lsp.get_buffers_by_client_id(client_id)
        for _,buf in ipairs(bufs) do
          semantic.setup(buf)
  		    inlay.set_inlay_hints(buf)
  		    codelens.refresh(buf,true)
        end
      end)
   else
        --local bufs = lsp.get_buffers_by_client_id(client_id)
			  --for _,win in ipairs(api.nvim_list_wins()) do
				--  if vim.tbl_contains(bufs,api.nvim_win_get_buf(win)) then
				--      semantic.win_highlight(win)
				--  end
        --end
   end

  if call_back ~= nil then
      call_back(client_id,result)
  end
end

function M.set_callback(fcn)
  call_back = fcn
end

function M.server_statuses()
  return last_result
end

return M
