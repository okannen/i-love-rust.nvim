local M = {}
local ui = require"i-love-rust.ui"
local rutils = require"i-love-rust.utils"
local semantic = require"i-love-rust.semantic"

function M.list_unsafe(all_workspace,bufnr)
  local file_name = vim.api.nvim_buf_get_name(bufnr)
  local unsafe_fun_decl = 0
  local unsafe_fun_call = 0
  local unsafe_trait_decl = 0
  local unsafe_trait_impl = 0
  rutils.with_current_workspace_source_file_list(bufnr,function(file_list)
    local cur_file = 1
    if not file_list or #file_list == 0 then
      return
    end
    local function rec_files(unsafe_list)
      local file = file_list[cur_file]
      local uri = vim.uri_from_fname(file)
      local existed =vim.fn.bufexists(file)
      local buffer = vim.fn.bufadd(file)
      vim.fn.bufload(buffer)
      semantic.with_buffer_semantic(buffer,function(tokens)
        for _,tok in ipairs(tokens) do
          if not tok:is_a("keyword") and tok:has_modifier("unsafe") then
            local item = tok:get_location(uri)
            if tok:is_a("interface") then
              if tok:has_modifier("declaration") then
                unsafe_trait_decl = unsafe_trait_decl + 1
                item.label = "trait "..item.label
              else
                unsafe_trait_impl = unsafe_trait_impl + 1
                item.label = "impl  "..item.label
              end
            elseif tok:has_modifier("declaration") then
                unsafe_fun_decl = unsafe_fun_decl + 1
                item.label = "fn    "..item.label
            else
                unsafe_fun_call = unsafe_fun_call + 1
                item.label = "call  "..item.label
            end
            table.insert(unsafe_list,item)
          end
        end
        if not existed then
          vim.cmd("bdelete "..buffer)
        end
        if cur_file ~= #file_list then
          cur_file = cur_file+1
          vim.schedule(function() rec_files(unsafe_list) end)
        else
          -- sort the token name, then first declaration followed by call/impl
          -- call and impl sorted by file, line
          table.sort(unsafe_list,
            function(a,b)
              if a.label:sub(6) == b.label:sub(6) then
                if a.label == b.label then
                  if a.uri == b.uri then
                    return a.range.start.line < b.range.start.line
                  else
                    return a.uri < b.uri
                  end
                else
                  return a.label > b.label
                end
              else
                return a.label < b.label
              end
            end)
          ui.locations_view(unsafe_list)
          --totaly arbitrary, but looks quite optimistic
          local minutes = 10*unsafe_fun_decl + 20*unsafe_fun_call + 10*unsafe_trait_decl + 20*unsafe_trait_impl
          local hours = math.ceil(minutes/60)
          local days = math.floor(hours/8)
          hours = hours - 8*days
          if days == 0 then
            print(string.format("Estimated time to review unsafe code: %d hours.",days))
          else
            print(string.format("Estimated time to review unsafe code: %d days and %d hours.",days,hours))
          end
          print(string.format("unsafe: %d fn, %d calls, %d trait, %d trait impl."
            ,unsafe_fun_decl,unsafe_fun_call,unsafe_trait_decl,unsafe_trait_impl))
        end
      end)
    end
    vim.schedule(function()rec_files({})end)
  end,
  function (package_dir)
    return all_workspace or (file_name:len() > package_dir:len() and file_name:sub(1,package_dir:len()) == package_dir)
  end
  )
end

return M
