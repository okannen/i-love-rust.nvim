local lsp = vim.lsp
local util = lsp.util
local manage_error = require'i-love-rust.error'.manage_error

local M = {}

function M.external_docs()
  lsp.buf_request(
    0,
    'experimental/externalDocs',
    util.make_position_params(),
    function(e,res)
      if e then
	manage_error(e)
	return
      end
      if res then
      	print("opening "..res)
        vim.loop.spawn( vim.g.i_love_rust_open_cmd,
            {args = {res},
                stdio = {nil,nil,nil}
            })
      end
    end)
end

return M
