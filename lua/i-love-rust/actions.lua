local util = vim.lsp.util
local rutils = require'i-love-rust.utils'
local ui = require'i-love-rust.ui'
local manage_error = require'i-love-rust.error'.manage_error

local M = {}

-- rustc bug fixed => no more space in test names
--local function escapeSpace(s)
--    local ret = "";
--    for match in (s.." "):gmatch("(.-) ") do
--        ret = ret .. match .. "\\ ";
--    end
--    return ret:sub(1,-3)
--end


local function make_cargo_command(args, opt_args)
    local cmd = " "

    local dir = args.workspaceRoot;

    cmd = string.format("cd '%s' && cargo ", dir)

    for _, value in ipairs(args.cargoArgs) do cmd = cmd .. value .. " " end

    for _, value in ipairs(args.cargoExtraArgs) do cmd = cmd .. value .. " " end

    if not vim.tbl_isempty(args.executableArgs) then
        cmd = cmd .. "-- "
        for _, value in ipairs(args.executableArgs) do
            cmd = cmd .. "\"" .. value .. "\"" .. " "
        end
    end

    if opt_args ~= nil and not vim.tbl_isempty(opt_args) then
        if vim.tbl_isempty(args.executableArgs) then
            cmd = cmd .. "-- "
        end
        for _, value in ipairs(opt_args) do
            cmd = cmd .. value .. " "
        end
    end

    return cmd
end

local run_buffer = nil


function M.run_cargo(argument, opt_args)

    -- check if a buffer with the latest id is already open, if it is then
    -- delete it and continue
    rutils.delete_buf(run_buffer)

    -- create the new buffer
    run_buffer = vim.api.nvim_create_buf(false, true)

    -- split the window to create a new buffer and set it to our window
    rutils.split(false, run_buffer)

    -- make the new buffer smaller
    rutils.resize(false, "-5")

    -- close the buffer when escape is pressed :)
    vim.api.nvim_buf_set_keymap(run_buffer, "n", "<Esc>", ":q<CR>", {})

    local command = make_cargo_command(argument.args, opt_args)

    -- run the command
    vim.fn.termopen(command)

    -- when the buffer is closed, set the latest buf id to nil else there are
    -- some edge cases with the id being sit but a buffer not being open
    local function onDetach(_, _) run_buffer = nil end
    vim.api.nvim_buf_attach(run_buffer, false, {on_detach = onDetach})
end

local function show_references(argument)

  ui.locations_view(argument)

end

function M.debug(argument, exec_args)
  require'i-love-rust.dap'.start(argument.args, exec_args)
end

function M.edit(edit)
    local tab_stop = rutils.apply_workspace_edit(edit)
    if tab_stop then
        util.jump_to_location(tab_stop)
    end
end

local function resolve_code_action(f, action, bufnr)
      bufnr = bufnr or 0
      vim.lsp.buf_request(bufnr, "codeAction/resolve", action, function(e,resolved_action)
        print("error",vim.inspect(e))
        if e then
          manage_error(e)
          return
        end
        f(resolved_action)
      end)
end

local function apply_action(action)

          if action.edit then
		                M.edit(action.edit)
          end

          if action.command == "rust-analyzer.gotoLocation" then
                  util.jump_to_location(action.arguments[1])
          elseif action.command == "rust-analyzer.showReferences" then
                show_references(action.arguments[3])
          elseif action.command == "rust-analyzer.runSingle" then
                  M.run_cargo(action.arguments[1])
          elseif action.command == "rust-analyzer.debugSingle" then
                   M.debug(action.arguments[1])
          elseif action.command then
            vim.notify("i-love-rust does not support language server command:" .. action.command,vim.log.levels.ERROR)
          end
end


function M.commands_to_callbacks(commands, command_lst)
    local command_list = command_lst or {}
    if type(commands) ~= "table" then
      return {}
    end
    for _, action in ipairs(commands) do
         table.insert(command_list,{
           message = action.title,
           group = action.group,
           kind = action.kind,
           isPreferred = action.isPreferred,
           callback = function()
                  if action.command or action.edit then
                    apply_action(action)
                  else
                    resolve_code_action(apply_action,action)
                  end
                end
         })
    end
    return command_list
end

return M
