local M = {}
local nvim_set_keymap = vim.api.nvim_set_keymap
local nvim_buf_set_keymap = vim.api.nvim_buf_set_keymap
local rutils = require'i-love-rust.utils'

local rust_action_maps = {
  ["code actions"] = {
    {"n","require'i-love-rust.hover_actions'.on_code_action_key()"},
    {"v","require'i-love-rust.hover_actions'.on_code_action_key()"}
  },
  ["hover"] = {
    {"n","require'i-love-rust.hover_actions'.on_hover_key()"},
    {"v","require'i-love-rust.hover_actions'.on_hover_key()"}
  },
  ["open external doc"] = {
    {"n","require'i-love-rust.external_documentation'.external_docs()"},
  },
  ["parent module"] = {
    {"n","require'i-love-rust.parent_module'.parent_module()"},
  },
  ["quickfix"]= {
    {"n","require'i-love-rust.hover_actions'.on_quickfix_key()"},
    {"v","require'i-love-rust.hover_actions'.on_quickfix_key()"}
  },
  ["debug"]= {
    {"n","require'i-love-rust.codelens'.run(\"rust-analyzer.debugSingle\")"},
  },
  ["run"]= {
    {"n","require'i-love-rust.codelens'.run(\"rust-analyzer.runSingle\")"},
  },
  ["related tests"]= {
    {"n","require'i-love-rust.runnables'.related_tests()"},
  },
  ["join lines"]= {
    {"n","require'i-love-rust.join_lines'.join_lines()"},
  },
  ["move item up"]= {
    {"n","require'i-love-rust.move_item'.move_item(true)"},
  },
  ["move item down"]= {
    {"n","require'i-love-rust.move_item'.move_item()"},
  },
  ["list unsafe"] = {
    {"n","require'i-love-rust.audit_unsafe'.list_unsafe()"},
  },
  ["list workspace unsafe"] = {
    {"n","require'i-love-rust.audit_unsafe'.list_unsafe(true)"},
  },
}
local lsp_action_maps = {
  ["code actions"] = {
    {"n","<cmd> lua vim.lsp.buf.code_action()"},
    {"v",":lua vim.lsp.buf.range_code_action()"}
  },
  ["declaration"] = {
    {"n","<cmd> lua require'i-love-rust.references'.locations_of('declaration')"},
  },
  ["definition"] = {
    {"n","<cmd> lua require'i-love-rust.references'.locations_of('definition')"},
  },
  ["implementation"] = {
    {"n","<cmd> lua require'i-love-rust.references'.locations_of('implementation')"},
  },
  ["type definition"] = {
    {"n","<cmd> lua require'i-love-rust.references'.locations_of('typeDefinition')"},
  },
  ["line diagnostics"] = {
    {"n","<cmd> lua vim.diagnostic.open_float()"},
  },
  ["hover"] = {
    {"n","<cmd> lua vim.lsp.buf.hover()"},
  },
  ["incoming calls"] = {
    {"n","<cmd> lua vim.lsp.buf.incoming_calls()"},
  },
  ["outgoing calls"] = {
    {"n","<cmd> lua vim.lsp.buf.outgoing_calls()"},
  },
  ["next diagnostic"] = {
    {"n","<cmd> lua vim.lsp.diagnostic.goto_next()"},
  },
  ["prev diagnostic"] = {
    {"n","<cmd> lua vim.lsp.diagnostic.goto_prev()"},
  },
  ["diagnostic locations"] = {
    {"n","<cmd> lua vim.lsp.diagnostic.set_loclist()"},
  },
  ["workspace diagnostic locations"] = {
    {"n","<cmd> lua vim.lsp.diagnostic.set_loclist({workspace=true})"},
  },
  ["error locations"] = {
    {"n","<cmd> lua vim.lsp.diagnostic.set_loclist({severity=\"Error\"})"},
  },
  ["workspace error locations"] = {
    {"n","<cmd> lua vim.lsp.diagnostic.set_loclist({severity=\"Error\",workspace=true})"},
  },
  ["references"] = {
    {"n","<cmd> lua require'i-love-rust.references'.references()"},
  },
  ["rename"] = {
    {"n","<cmd>lua vim.lsp.buf.rename()"},
  },
  ["signature help"] = {
    {"n","<cmd>lua vim.lsp.buf.signature_help()"},
  },
  ["fold"] = {
    {"n","<cmd>lua require'i-love-rust.folding_range'.setup_lsp_fold()"},
  },
  ["insert mode signature help"] = {
    {"i","<cmd>lua vim.lsp.buf.signature_help()"},
  },
  ["select containing"] = {
    {"n","<cmd>lua require\'i-love-rust.selection_range\'.select_containing()"},
    {"v","<cmd>lua require\'i-love-rust.selection_range\'.select_containing()"},
  },
  ["token semantic"] = {
    {"n","<cmd>lua require\'i-love-rust.semantic\'.current()"},
  },
  ["undo select containing"] = {
    {"n","<cmd>lua require\'i-love-rust.selection_range\'.undo_select_containing()"},
    {"v","<cmd>lua require\'i-love-rust.selection_range\'.undo_select_containing()"},
  },
  ["debugger scopes"] = {
    {"n","<cmd>lua require\'dap.ui.widgets\'.sidebar(require\'dap.ui.widgets\'.scopes).open()"},
  },
  ["debugger frames"] = {
    {"n","<cmd>lua require\'dap.ui.widgets\'.sidebar(require\'dap.ui.widgets\'.frames).open()"},
  },
  ["debugger expression"] = {
    {"n","<cmd>lua require\'i-love-rust.keymap\'.dap_hover()"},
    {"v","<cmd>lua require\'i-love-rust.keymap\'.dap_hover()"},
  },
  ["debugger terminal"] = {
    {"n","<cmd>lua require\'dap\'.repl.open()"},
  },
  ["debugger toggle breakpoint"] = {
    {"n","<cmd>lua require\'dap\'.toggle_breakpoint()"},
  },
  ["debugger n-hits breakpoint"] = {
    {"n",":<C-U>lua require'i-love-rust.keymap'.on_nhit_breakpoint_key()"},
  },
  ["debugger continue"] = {
    {"n","<cmd>lua require\'dap\'.continue()"},
  },
  ["debugger step over"] = {
    {"n","<cmd>lua require\'dap\'.step_over()"},
  },
  ["debugger step out"] = {
    {"n","<cmd>lua require\'dap\'.step_out()"},
  },
  ["debugger step into"] = {
    {"n","<cmd>lua require\'dap\'.step_into()"},
  },
  ["debugger stacktrace up"] = {
    {"n","<cmd>lua require\'dap\'.up()"},
  },
  ["debugger stacktrace down"] = {
    {"n","<cmd>lua require\'dap\'.down()"},
  },
  ["debugger to cursor"] = {
    {"n","<cmd>lua require\'dap\'.run_to_cursor()"},
  },
}

local function set_key(mode,lhs,code)
  nvim_set_keymap(mode,lhs,code.."<CR>",{silent=true})
end

local function set_rust_key(buffer,mode,lhs,code)
  nvim_buf_set_keymap(buffer,mode,lhs,"<cmd> lua "..code.."<CR>",{silent=true})
end

function M.setup_global_mapping(options)
  for map,action in pairs(options) do
    for _,mapings in ipairs(lsp_action_maps[action] or {}) do
      set_key(mapings[1],map,mapings[2])
    end
  end
end

function M.setup_rust_mapping(options,buffer)
  buffer = buffer or 0
  for map,action in pairs(options) do
    for _,mapings in ipairs(rust_action_maps[action] or {}) do
      set_rust_key(buffer,mapings[1],map,mapings[2])
    end
  end
end

function M.on_nhit_breakpoint_key()
  local count = vim.v.count
  if count == 0 then
    count = nil
  end
  require'dap'.set_breakpoint(nil,count)
end

function M.dap_hover()
    local expr = nil
    if rutils.is_range_selection_mode() then
      local start,_end = rutils.get_selection_range()
      local lines = vim.fn.getline(start[1],_end[1])
	    local id_last = #lines
      lines[id_last] = lines[id_last]:sub(1,_end[2]+1)
      lines[1] = lines[1]:sub(start[2]-1)
      expr = vim.fn.join(lines, "\n")
    end
    require'dap.ui.widgets'.hover(expr)
end


return M
