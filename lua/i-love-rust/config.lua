local vim = vim

local M = {}

local package_root=string.sub(debug.getinfo(1).source,2,-27)

local defaults = {
    tools = { -- i-love-rust options
        -- show function signature in hover when the cursor is on '(' or after it in insert mode
        -- and show parameter name after or on ',' character
        auto_signature_help = true,

        --- highlight all occurence of the entity under the cursor
        setup_document_highlight = true,
        

        -- maps keymap => functionnality
        -- see README.md
        keymaps = {},

        -- if not nil, use rust-analyzer semantic to highlight code
        highlight = {
            -- which function for highliting, default to require"i-love-rust".semantic.simple_highlighter
            --  param: token
            --    token.type => string, token type
            --    token.modifiers => list of token modifiers
            --    token.modifiers:contains(modifier) => return true if modifier in modifiers list
            --    token:highlight(group) => apply higlight group to token
            --    token:get_text() => return the token string
            --  exemple => see `simple_highlighter` in module "i-love-rust/semantic" 
            --  to see list of types and modifiers, run the command
            --    :lua print(vim.inspect(require"i-love-rust.semantic".legend))
            token_highlighter = nil,

            -- shall vim syntax be kept
            overlay_syntax = false,

            -- rust-analyzer may be long, before it has finish to compute
            -- code semantic use this syntax file
            temporary_highlight_syntax = package_root.."syntax/simple_rust.vim",
          },

        -- automatically set inlay hints (type hints)
        -- There is an issue due to which the hints are not applied on the first
        -- opened file. For now, write to the file to trigger a reapplication of
        -- the hints or just run :RustSetInlayHints.
        -- default: true
        autoSetHints = true,

        -- automatically set code lenses 
        -- There is an issue. For now, write to the file to trigger a reapplication of
        -- the code lenses or just run :RustSetCodeLens.
        -- default: true
        autoSetCodeLenses = true,

        -- whether to show hover actions inside the hover window
        -- this overrides the default hover handler so something like lspsaga.nvim's hover would be overriden by this
        -- default: true
        hover_with_actions = true,

        -- These apply to the default RustRunnables command
        runnables = {
            -- whether to use telescope for selection menu or not
            -- default: true
            use_telescope = true

            -- rest of the opts are forwarded to telescope
        },

        -- These apply to the default RustSetInlayHints command
        inlay_hints = {

            -- Only show inlay hints for the current line
            only_current_line = false,

            -- Event which triggers a refersh of the inlay hints.
            -- You can make this "CursorMoved" or "CursorMoved,CursorMovedI" but
            -- not that this may cause  higher CPU usage.
            -- This option is only respected when only_current_line and
            -- autoSetHints both are true.
            only_current_line_autocmd = "CursorHold",

            -- wheter to show parameter hints with the inlay hints or not
            -- default: true
            show_parameter_hints = true,

            -- prefix for parameter hints
            -- default: "<-"
            parameter_hints_prefix = "    ",

            -- prefix for all the other hints (type, chaining)
            -- default: "=>"
            other_hints_prefix = "   :",

            -- whether to align to the lenght of the longest line in the file
            max_len_align = false,

            -- padding from the left if max_len_align is true
            max_len_align_padding = 1,

            -- whether to align to the extreme right or not
            right_align = false,

            -- padding from the right if right_align is true
            right_align_padding = 7,

            -- The color of the hints
            highlight = "InlayHints"
        },

        hover_actions = {
            -- the border that is used for the hover window
            -- see vim.api.nvim_open_win()
            border = {
                {"╭", "FloatBorder"}, {"─", "FloatBorder"},
                {"╮", "FloatBorder"}, {"│", "FloatBorder"},
                {"╯", "FloatBorder"}, {"─", "FloatBorder"},
                {"╰", "FloatBorder"}, {"│", "FloatBorder"}
            },

            -- whether the hover action window gets automatically focused
            -- default: false
            auto_focus = false,

            -- weither code_actions appear in floating window (true) or
            -- in a dedicated window
            float_code_actions = false,

            -- weither hover appear in floating window
            float_hover = true
        },
        dap_adapter = {
          type = 'executable',
          command = 'lldb-vscode',
          name = "rt_lldb"
        }
    },

    -- all the opts to send to nvim-lspconfig
    -- these override the defaults set by i-love-rust.nvim
    -- see https://github.com/neovim/nvim-lspconfig/blob/master/CONFIG.md#rust_analyzer
 -- rust-analyer options
	server = {
        on_init = function(client, init_res)
          if init_res.offsetEncoding == "utf-8" then
            client.offset_encoding = "utf-8"
          end
        end,
    		on_attach = function(_, buffer)
          vim.schedule(function()
    		  local inlay = require('i-love-rust.inlay_hints')
    		  local codelens = require('i-love-rust.codelens')
    		  local semantic = require('i-love-rust.semantic')
    		  local hover_actions = require('i-love-rust.hover_actions')
    		  local keymap = require('i-love-rust.keymap')
          local signature_help = require('i-love-rust.signature')
        
          keymap.setup_rust_mapping(M.options.tools.keymaps)
          signature_help.setup()
			--for _,win in ipairs(api.nvim_list_wins()) do
			--	if api.nvim_win_get_buf(win) == buffer then
			--	      semantic.client_win_highlight(client,win)
			--	end
			--end
          codelens.setup_autocmd(buffer)
          inlay.setup_autocmd(buffer)

    			semantic.setup(buffer)
    			inlay.set_inlay_hints(buffer)
    			codelens.refresh(buffer)
          hover_actions.set_up(buffer)
    		end)
      end,


		settings = {
			["rust-analyzer"] ={
				hoverActions = {
					references=true,
					},
				lens = {
					methodReferences = true,
					},
				}
			}
		}
}

M.options = {}

function M.setup(options)
    M.options = vim.tbl_deep_extend("force", {}, defaults, options or {})
    if options.tools.highlight ~= nil then
      M.options.tools.highlight = options.tools.highlight
    end
end

function M.show_key_map()
  local ui=require"i-love-rust.ui"
  local text = {}
  local spaces = "    "
  local keys = {}
  for k,_ in pairs(M.options.tools.keymaps) do
    table.insert(keys, k)
  end
  table.sort(keys)
  for _,k in ipairs(keys) do
    local fun = M.options.tools.keymaps[k]
    table.insert(text,string.format("\"%s\" %s=> %s",k,spaces:sub(k:len()),fun))
  end
  ui.open_floating_window("key help",text,{},true)
end


return M
