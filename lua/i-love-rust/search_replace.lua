local M = {}
local util = vim.lsp.util
local rutil = require'i-love-rust.utils'
local manage_error = require'i-love-rust.error'.manage_error

function M.search_replace_lines(query, start_line, end_line, parse_only)
  local range = nil
  if rutil.is_range_selection_mode() then
    range = rutil.make_given_range_params()
  elseif start_line ~=nil and end_line ~= nil then
     range = {{  start = rutil.to_position({start_line,0}),
        ["end"] = rutil.to_position({end_line+1,0})}}
  end
  M.search_replace(query,range,parse_only)
end

function M.search_replace(query, ranges, parse_only)
  parse_only = parse_only or false
  local ssr_params = util.make_position_params()
  ssr_params.query = query
  ssr_params.parseOnly = parse_only
  ssr_params.selections = ranges
  vim.lsp.buf_request(0,"experimental/ssr",ssr_params,
    function (err,result)
      if err ~= nil then
	    manage_error(err)
      elseif not parse_only then
        util.apply_workspace_edit(result)
        local count_changes = 0;
        local count_files = 0;
        for _,f in pairs(result) do
          count_changes = count_changes + #f
          count_files = count_files + 1
        end
        vim.notify(string.format("%d changes in %d files",count_changes,count_files))
      else
         vim.notify("search replace syntax correct")
      end
    end
    )
end

return M;
