local lsp = vim.lsp
local manage_error = require'i-love-rust.error'.manage_error
local rutils = require'i-love-rust.utils'

local M = {}
M.previous = {}

local function with_selection_range(f, position)
	if position == nil then
		position = lsp.util.make_position_params()
		position.positions = {position.position}
		position.position = nil
	end
	lsp.buf_request(0,'textDocument/selectionRange', position, function(e,res)
		if e ~= nil then
			manage_error(e)
			return
		end
		f(res[1])
	end
		)
end

local function select_range(range)
	local st = rutils.from_position(range.start)
	local ed = rutils.from_position(range["end"])
	if vim.o.selection ~= 'exclusive' then
	    if ed[2] ~= 1 then
		ed[2] = ed[2] - 1
	    end
	end
	vim.fn.setpos('.',{0,ed[1], ed[2]})
	vim.cmd("normal! v")
	vim.fn.setpos('.',{0,st[1], st[2]})
end

-- return -1 if less, 0 if equal, 1 if greater
local function cmp_pos(pos1, pos2)
	if pos1.line == pos2.line then
		if pos1.character < pos2.character then
			return -1
		elseif pos1.character > pos2.character then
			return 1
		else
			return 0
		end
	elseif pos1.line < pos2.line then
		return -1
	else
		return 1
	end
end

local function is_strict_super_range(sup, sub)
	local st_cmp = cmp_pos(sup.start, sub.start)
	local ed_cmp = cmp_pos(sup["end"], sub["end"])
	if st_cmp == -1 then
		return ed_cmp >= 0
	elseif st_cmp == 0 then
		return ed_cmp == 1
	else
		return false
	end
end

function M.select_containing()
    with_selection_range(
        function(selection_ranges)
	    	if not rutils.is_range_selection_mode() then
			if selection_ranges.parent then
				select_range(selection_ranges.parent.range)
			end
			M.previous = { {position = vim.fn.getpos('.')}}
		elseif selection_ranges.parent then
			selection_ranges = selection_ranges.parent
			local cur_range = rutils.make_given_range_params()
			while selection_ranges.parent do
				selection_ranges = selection_ranges.parent
				if is_strict_super_range(selection_ranges.range, cur_range.range) then
					if M.previous ~= nil then
						table.insert(M.previous, {range=cur_range.range})
					else
						M.previous = {{range = cur_range.range}}
					end
					vim.cmd("normal! v")
					select_range(selection_ranges.range)
					return
				end
			end
		end
         end
	)
end

function M.undo_select_containing()
	if #M.previous == 0 then
		return
	end
        local last = M.previous[#M.previous]
        M.previous[#M.previous] = nil
	if last.position then
		vim.fn.setpos('.',last.position)
	else
		if rutils.is_range_selection_mode() then
			vim.cmd("normal! v")
		end
		select_range(last.range)
	end
end


return M
