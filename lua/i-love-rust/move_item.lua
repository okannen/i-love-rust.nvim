-- ?? helps with all the warnings spam
local vim = vim
local rutils = require('i-love-rust.utils')
local manage_error = require'i-love-rust.error'.manage_error

local M = {}

local function get_params(up)
    local direction = up and "Up" or "Down"
    local params = vim.lsp.util.make_range_params()
    params.direction = direction

    return params
end

-- move it baby
local function handler(e, result)
    if e then
	    manage_error(e)
	    return
    end
    if result == nil then return end
    rutils.snippet_text_edits_to_text_edits(result)
    vim.lsp.util.apply_text_edits(result)
end

-- Sends the request to rust-analyzer to move the item and handle the response
function M.move_item(up)
    vim.lsp.buf_request(0, "experimental/moveItem", get_params(up or false), handler)
end

return M
