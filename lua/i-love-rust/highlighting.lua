local M = {}

function M.simple_highlighter(token)
  if token:is_a("comment") then
    token:highlight("Grey")
  elseif token:has_modifier("documentation") or token:has_modifier("injected") then
    if token:has_modifier("attribute") then
      token:highlight("AzureB3")
    elseif token:is_a("number") then
      token:highlight("MagentaB2")
    elseif token:is_a("boolean") then
      token:highlight("MagentaB2")
    elseif token:is_a("string") then
      token:highlight("GreenB2")
    elseif token:is_a("character") then
      token:highlight("ChartreuseGreenB2")
    elseif token:is_a("macro") then
      token:highlight("CyanB2")
    elseif token:is_a("interface") then
      token:highlight("BlueB2")
    elseif token:has_modifier("unsafe") then
      token:highlight("SalmonB2")
    else
      token:highlight("Grey")
    end
  else
    if token:has_modifier("attribute") then
      token:highlight("OrangeB2")
    elseif token:is_a("number") then
      token:highlight("MagentaF1")
    elseif token:is_a("boolean") then
      token:highlight("MagentaF1")
    elseif token:is_a("string") then
      token:highlight("GreenF1")
    elseif token:is_a("character") then
      token:highlight("ChartreuseGreenF1")
    elseif token:is_a("macro") then
      token:highlight("CyanF1")
    elseif token:is_a("interface") then
      token:highlight("BlueF1")
    elseif token:has_modifier("unsafe") then
      token:highlight("Salmon")
    end
  end

  if token:is_a("keyword") then
	  if token:get_text() == "impl" then
      token:highlight("Bold")
    elseif token:has_modifier("controlFlow") then
      token:highlight("Italic")
    end
    return
  end

  if token:has_modifier("declaration") then
    if token:is_a("selfKeyword") or token:is_a("parameter") or token:is_a("typeParameter")
      or token:is_a("variable") or token:is_a("lifetime") then
      token:highlight("Bold")
      if token:has_modifier("documentation") or token:has_modifier("injected") then
        token:highlight("GreyB1")
      else
        token:highlight("GreyF3")
      end
    else
      token:highlight("Bold")
    end
    if token:has_modifier("public") then
      token:highlight("Underline")
    end
  elseif token:has_modifier("controlFlow") then
    token:highlight("Italic")
  elseif token:has_modifier("mutable") then
    token:highlight("Italic")
  elseif token:has_modifier("consuming") then
    token:highlight("Italic")
  end

end

function M.setup_higroups()
  local synIDattr = vim.fn.synIDattr
  local synIDtrans = vim.fn.synIDtrans
  local hlID = vim.fn.hlID

  local function byte_to_val(byte)
    if byte > 96 then
      return byte - 87
    elseif byte > 65 then
      return byte - 55
    else
      return byte - 48
    end
  end

  local function val_to_str(val)
    local u = math.fmod(val,16)
    local d =  math.floor(val/16)
    local function u_to_ch(v)
      if v < 10 then
        return string.char(v+48)
      else
        return string.char(v+55)
      end
    end
    return u_to_ch(d)..u_to_ch(u)
  end

  local function color_to_nums(color)
    return { 
      byte_to_val(color:byte(2))*16+byte_to_val(color:byte(3)),
      byte_to_val(color:byte(4))*16+byte_to_val(color:byte(5)),
      byte_to_val(color:byte(6))*16+byte_to_val(color:byte(6)),
    }
  end

  local function nums_to_color(nums)
    local res = "#"..val_to_str(nums[1])..val_to_str(nums[2])..val_to_str(nums[3])
    return res
  end

  local function fade_color(color,target,prop)
    prop = 1-prop
    local nc = color
    local function shift(a,b)
      local res = a+math.floor((b-a)*prop)
      return res
    end
    return nums_to_color{shift(nc[1],target[1]), shift(nc[2],target[2]), shift(nc[3], target[3])} 
  end

  local function define_mixed_color(group,cterm_group,color_a,color_b,prop)
    local ctermfg = synIDattr(synIDtrans(hlID(cterm_group)),"fg","cterm")
    local color = fade_color(color_a,color_b,prop)
    vim.cmd("hi def "..group.." ctermfg="..ctermfg.." guifg="..color)
  end

  local function style_group(group,style)
    vim.cmd("hi def "..group.." cterm="..style.." gui="..style)
  end

  vim.cmd([[
  hi def Red ctermfg=DarkRed guifg=#ea6963
  hi def Yellow ctermfg=DarkYellow guifg=#d8a657
  hi def Green ctermfg=DarkGreen guifg=#a9b665
  hi def Blue ctermfg=DarkBlue guifg=#7daea3
  hi def Normal ctermfg=White guifg=#d4be98 ctermbg=Black guibg=#32302f
  ]])

  if hlID("Purple") ~= 0 then
    vim.cmd("hi def link Magenta Purple")
  else
    vim.cmd("hi def Magenta ctermfg=DarkMagenta guifg=#d3869b")
  end

  if hlID("Aqua") ~= 0 then
    vim.cmd("hi def link Cyan Aqua")
  else
    vim.cmd("hi def Cyan ctermfg=DarkCyan guifg=#89b482")
  end

  local fg = color_to_nums(synIDattr(synIDtrans(hlID("Normal")),"fg#","gui"))
  local bg = color_to_nums(synIDattr(synIDtrans(hlID("Normal")),"bg#","gui"))

  local red = color_to_nums(synIDattr(synIDtrans(hlID("Red")),"fg#","gui"))
  local yellow = color_to_nums(synIDattr(synIDtrans(hlID("Yellow")),"fg#","gui"))
  local green = color_to_nums(synIDattr(synIDtrans(hlID("Green")),"fg#","gui"))
  local cyan = color_to_nums(synIDattr(synIDtrans(hlID("Cyan")),"fg#","gui"))
  local blue = color_to_nums(synIDattr(synIDtrans(hlID("Blue")),"fg#","gui"))
  local magenta = color_to_nums(synIDattr(synIDtrans(hlID("Magenta")),"fg#","gui"))

  define_mixed_color("Grey","Normal",fg,bg,0.5)
  define_mixed_color("Orange","Yellow",red,yellow,0.5)
  define_mixed_color("ChartreuseGreen","Green",yellow,green,0.5)
  define_mixed_color("SpringGreen","Cyan",green,cyan,0.5)
  define_mixed_color("Azure","Blue",cyan,blue,0.5)
  define_mixed_color("Violet","Magenta",blue,magenta,0.5)
  define_mixed_color("Rose","Red",magenta,red,0.5)

  local orange = color_to_nums(synIDattr(synIDtrans(hlID("Orange")),"fg#","gui"))
  define_mixed_color("Salmon","Red",red,orange,0.5)

  local function define_nuances(group)
    local ctermfg = synIDattr(synIDtrans(hlID(group)),"fg","cterm")
    local guifg = synIDattr(synIDtrans(hlID(group)),"fg#","gui")
    --vim.cmd("hi! def "..group.." ctermfg="..ctermfg.." guifg="..guifg)
    guifg = color_to_nums(guifg)
    vim.cmd("hi def "..group.."B1 ctermfg="..ctermfg.." guifg="..fade_color(guifg,bg,0.833))
    vim.cmd("hi def "..group.."B2 ctermfg="..ctermfg.." guifg="..fade_color(guifg,bg,0.667))
    vim.cmd("hi def "..group.."B3 ctermfg="..ctermfg.." guifg="..fade_color(guifg,bg,0.5))
    vim.cmd("hi def "..group.."B4 ctermfg="..ctermfg.." guifg="..fade_color(guifg,bg,0.333))
    vim.cmd("hi def "..group.."B5 ctermfg="..ctermfg.." guifg="..fade_color(guifg,bg,0.167))
    vim.cmd("hi def "..group.."F1 ctermfg="..ctermfg.." guifg="..fade_color(guifg,fg,0.833))
    vim.cmd("hi def "..group.."F2 ctermfg="..ctermfg.." guifg="..fade_color(guifg,fg,0.667))
    vim.cmd("hi def "..group.."F3 ctermfg="..ctermfg.." guifg="..fade_color(guifg,fg,0.5))
    vim.cmd("hi def "..group.."F4 ctermfg="..ctermfg.." guifg="..fade_color(guifg,fg,0.333))
    vim.cmd("hi def "..group.."F5 ctermfg="..ctermfg.." guifg="..fade_color(guifg,fg,0.167))
  end

  for _,c in ipairs({"Red","Salmon","Orange","Yellow","ChartreuseGreen","Green","SpringGreen",
      "Cyan","Azure","Blue","Violet","Magenta","Rose","Grey","Salmon"}) do
    define_nuances(c)
  end

  style_group("Italic","italic")
  style_group("Bold","bold")
  style_group("Underline","underline")
  style_group("StrikeThrough","strikethrough")
  style_group("Inverse","inverse")
  style_group("Reverse","reverse")
end

function M.setup()
  vim.cmd("au ColorScheme,BufEnter * lua require'i-love-rust.highlighting'.setup_higroups()")
  M.setup_higroups()
end


return M
