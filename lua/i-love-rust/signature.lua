local lsp = vim.lsp
local manage_error = require'i-love-rust.error'.manage_error
local util = lsp.util
local ui = require'i-love-rust.ui'
local api = vim.api
local options = require'i-love-rust.config'.options.tools

local M = {}

M.folds = {}

function M.signature_help_after_insert()
  local param = util.make_position_params()
  local char
  local pos = vim.fn.getpos('.')
  if vim.fn.mode():sub(1,1) == 'i' and pos[3] > 0 then
    char = vim.fn.getline(pos[2]):sub(pos[3]-1,pos[3]-1)
  else
    param.position.character= param.position.character+1
    char = vim.fn.getline(pos[2]):sub(pos[3],pos[3])
  end
  if char == '(' or char == ',' then
    lsp.buf_request(0,'textDocument/signatureHelp',
	    param,
      function(e,result)
	      if e ~= nil then
	      	manage_error(e)
	      	return
	      end
        if not result or not result.signatures or #result.signatures == 0 then
          return
        end
        if char == '(' then
          ui.open_floating_window("signature help",{result.signatures[1].label},{},false)
        else
          local signature = result.signatures[1]
          local param_num = signature.active_parameter
          if param_num ~= nil then
            param = signature.parameters[param_num+1]
            ui.open_floating_window("signature help",{param.label},{},false)
          end
        end

	    end)
    end
end

function M.enable_auto_signature_help(buffer)
  if not buffer or buffer == 0 then
    buffer = api.nvim_get_current_buf()
  end
  vim.cmd(string.format([[
    augroup i_love_rust_signature_help
      au CursorMoved,CursorMovedI,TextChangedI <buffer=%d> lua require"i-love-rust.signature".signature_help_after_insert()
    augroup END
  ]],buffer))
end

function M.disable_auto_signature_help(buffer)
  if not buffer or buffer == 0 then
    buffer = api.nvim_get_current_buf()
  end
  vim.cmd([[
    augroup i_love_rust_signature_help
      au! * <buffer=%d>
    augroup END
  ]])
end

function M.setup(buffer)
  if options.auto_signature_help then
    M.enable_auto_signature_help(buffer)
  end
end



return M
