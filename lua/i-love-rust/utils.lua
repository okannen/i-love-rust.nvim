local vim = vim
local api = vim.api
local util = vim.lsp.util
local uv = vim.loop

local bit = require"bit"

local M = {}

--@private
--- Position is a https://microsoft.github.io/language-server-protocol/specifications/specification-current/#position
--- Returns a zero-indexed column, since set_lines() does the conversion to
--- 1-indexed
local function get_line_byte_from_position(bufnr, position)
  -- LSP's line and characters are 0-indexed
  -- Vim's line and columns are 1-indexed
  local col = position.character
  -- When on the first character, we can ignore the difference between byte and
  -- character
  if col > 0 then
    if not api.nvim_buf_is_loaded(bufnr) then
      vim.fn.bufload(bufnr)
    end

    local line = position.line
    local lines = api.nvim_buf_get_lines(bufnr, line, line + 1, false)
    if #lines > 0 then
      local ok, result = pcall(vim.str_byteindex, lines[1], col)

      if ok then
        return result
      end
    end
  end
  return col
end

function M.delete_buf(bufnr)
    if bufnr ~= nil then vim.api.nvim_buf_delete(bufnr, {force = true}) end
end

function M.split(vertical, bufnr)
    local cmd = vertical and "vsplit" or "split"

    vim.cmd(cmd)
    local win = vim.api.nvim_get_current_win()
    vim.api.nvim_win_set_buf(win, bufnr)
end

function M.resize(vertical, amount)
    local cmd = vertical and "vertical resize " or "resize"
    cmd = cmd .. amount

    vim.cmd(cmd)
end

function M.is_bufnr_rust(bufnr)
    return vim.api.nvim_buf_get_option(bufnr, 'ft') == 'rust'
end

function M.to_position(cursor, buffer)
    local buf = buffer or 0
    local character = 0
    if cursor[2] > 0 then
    	character = vim.lsp.util.character_offset(buf,cursor[1]-1,cursor[2])
    end
    return {
        line = cursor[1]-1,
        character = character,
    }
end

function M.from_position(position, buffer)
    local buf = buffer or 0
    return {
       position.line + 1,
       get_line_byte_from_position(buf,position) + 1
       }
end

function M.make_text_document_params(buffer)
  return { uri = vim.uri_from_bufnr(buffer) }
end

function M.selection_range()
    local start_m = vim.api.nvim_buf_get_mark(0, "<")
    local end_m = vim.api.nvim_buf_get_mark(0, ">")

    return {start = M.to_position(start_m), ["end"] = M.to_position(end_m)}
end

function M.is_range_selection_mode()
    local mod = vim.fn.mode()
    return mod == "v" or mod == "V" or mod == "s" or mod == "S"
end

function M.get_selection_range()
  local p = vim.fn.getpos("v")
  local start_pos = {p[2],p[3]}

  p = vim.fn.getpos(".")
  local end_pos = {p[2],p[3]}

  if end_pos[1] < start_pos[1] or end_pos[1] == start_pos[1] and end_pos[2] < start_pos[2] then
      local start_pos_ = start_pos
      start_pos = end_pos
      end_pos = start_pos_
  end

  local A = start_pos
  local B = end_pos

  if vim.o.selection ~= 'exclusive' then
    B[2] = B[2] + 1
  end

  return A,B
end

function M.make_given_range_params(start_pos, end_pos)
  vim.validate {
    start_pos = {start_pos, 't', true};
    end_pos = {end_pos, 't', true};
  }
  if not start_pos then
      local p = vim.fn.getpos("v")
      start_pos = {p[2],p[3]}
  end
  if not end_pos then
      local p = vim.fn.getpos(".")
      end_pos = {p[2],p[3]}
  end

  if end_pos[1] < start_pos[1] or end_pos[1] == start_pos[1] and end_pos[2] < start_pos[2] then
      local start_pos_ = start_pos
      start_pos = end_pos
      end_pos = start_pos_
  end


  local A = start_pos
  local B = end_pos
  -- convert to 0-index
  A[1] = A[1] - 1
  B[1] = B[1] - 1
  A[2] = A[2] - 1
  B[2] = B[2] - 1
  -- account for encoding.
  if A[2] > 0 then
    A = {A[1], vim.lsp.util.character_offset(0, A[1], A[2])}
  end
  if B[2] > 0 then
    B = {B[1], vim.lsp.util.character_offset(0, B[1], B[2])}
  end
  -- we need to offset the end character position otherwise we loose the last
  -- character of the selection, as LSP end position is exclusive
  -- see https://microsoft.github.io/language-server-protocol/specification#range
  if vim.o.selection ~= 'exclusive' then
    B[2] = B[2] + 1
  end
  return {
    textDocument = vim.lsp.util.make_text_document_params(),
    range = {
      start = {line = A[1], character = A[2]},
      ['end'] = {line = B[1], character = B[2]}
    }
  }
end

local function manage_tab_stop(lines, start)
    local line_count = 0
    local tab_char = nil
    local tab_line = nil
    local size = nil
    local new_lines = {}
    for _,line in pairs(lines) do
        local res = string.find(line,"$0")
        if res ~= nil then
            line = line:gsub("$0","")
            tab_char = res
            tab_line = line_count
            size = 2
        else
            res = string.find(line,"${[^}]*}")
            if res ~= nil then
                tab_char = res
                tab_line = line_count
                local mtch = string.match(line,"${[^}]*:([^}]*)}")
                size = #mtch
                line = line:gsub("${[^}]*}",mtch)
            end
        end
        table.insert(new_lines,line)

        line_count = line_count+1
    end

    if tab_char ~= nil then
        tab_char = tab_char - 1
        local line = start.line+tab_line
        if tab_line == 0 then
            tab_char = start.character + tab_char
        end
        return new_lines,{start = {
                character = tab_char,
                line = line,
            },
            ["end"] = {
                character = tab_char + size,
                line = line
            }
        }
    end

    return new_lines,nil
end

local function sort_by_key(fn)
  return function(a,b)
    local ka, kb = fn(a), fn(b)
    assert(#ka == #kb)
    for i = 1, #ka do
      if ka[i] ~= kb[i] then
        return ka[i] < kb[i]
      end
    end
    -- every value must have been equal here, which means it's not less than.
    return false
  end
end

local edit_sort_key = sort_by_key(function(e)
  return {e.A[1], e.A[2], e.i}
end)

function M.apply_text_edits(text_edits, bufnr)
  if not next(text_edits) then return end
  if not api.nvim_buf_is_loaded(bufnr) then
    vim.fn.bufload(bufnr)
  end
  api.nvim_buf_set_option(bufnr, 'buflisted', true)
  local start_line, finish_line = math.huge, -1
  local cleaned = {}
  for i, e in ipairs(text_edits) do
    -- adjust start and end column for UTF-16 encoding of non-ASCII characters
    local start_row = e.range.start.line
    local start_col = get_line_byte_from_position(bufnr, e.range.start)
    local end_row = e.range["end"].line
    local end_col = get_line_byte_from_position(bufnr, e.range['end'])
    start_line = math.min(e.range.start.line, start_line)
    finish_line = math.max(e.range["end"].line, finish_line)

    local lines = vim.split(e.newText, '\n', true)
    local tab_stop_range = nil

    if e.insertTextFormat == 2 then
        lines, tab_stop_range = manage_tab_stop(lines,e.range.start)
    end
    table.insert(cleaned, {
      i = i;
      A = {start_row; start_col};
      B = {end_row; end_col};
      lines = lines;
      tab_stop = tab_stop_range and tab_stop_range.start
    })

  end

  table.sort(cleaned, edit_sort_key)

  table.insert(cleaned, {
    A = {finish_line+1, 0},
    B = {finish_line+1, 0},
    lines = {},
  })

  local lines = api.nvim_buf_get_lines(bufnr, start_line, finish_line + 1, false)

  local new_lines = {}
  local cur_line = 1
  local cur_char = 1
  local tab_stop = nil
  -- simple algorithm =>
  --  1. copie content from start of unmodified
  -- to start of edit,
  --  2. then copie edit content,
  --  3. then set start of
  -- unmodified to edit end.
  for _,edit in ipairs(cleaned) do

    --edit start
    local s_line = edit.A[1] - start_line + 1
    local s_char = edit.A[2] + 1

    -- 1. insert unmodified content to edit start
    --   a) insert unmodified unmodified chars at previous edit end line
    if s_line ~= cur_line then
      -- fill the line
      if #new_lines > 0 then
        new_lines[#new_lines] = new_lines[#new_lines]..string.sub(lines[cur_line], cur_char)
      else
        new_lines = {
          string.sub(lines[cur_line], cur_char)
        }
      end
      cur_line = cur_line+1
      cur_char = 1
      -- add lines up to edit first line
      if s_line ~= cur_line then
        vim.list_extend(new_lines, lines, cur_line, s_line-1)
      end
      cur_line = s_line
    end

    --   b) fill the line at edit first line
    if s_char ~= cur_char then
      if cur_char == 1 then
        table.insert(new_lines, string.sub(lines[cur_line],1, s_char-1))
      else
        new_lines[#new_lines] = new_lines[#new_lines]..string.sub(lines[cur_line], cur_char, s_char-1)
      end
    end

    local tabstop_ch = 0
    local tabstop_line = 0

    -- 2.a) insert edit text
    if #edit.lines > 0 then
      if #new_lines > 0 then
        new_lines[#new_lines] = new_lines[#new_lines]..edit.lines[1]
      else
        new_lines = {edit.lines[1]}
      end
      if #edit.lines > 1 then
        vim.list_extend(new_lines, edit.lines, 2)
      end
    end

    --  2.b) if there is a tab_stop compute its coordinates
    if edit.tab_stop then
      local ch = edit.tab_stop.character
      ch = vim.str_utfindex(new_lines[edit.tab_stop.line-start_line+1], ch)
      tab_stop = {
        line = edit.tab_stop.line,
        character = ch
      }
    end

    -- 3. set unmodified start to end of previous edit
    cur_line = edit.B[1] - start_line + 1
    cur_char = edit.B[2] + 1
  end

  api.nvim_buf_set_lines(bufnr, start_line, finish_line + 1, false, new_lines)
  return tab_stop
end
--- Applies a `TextDocumentEdit`, which is a list of changes to a single
--- document.
---
---@param text_document_edit table: a `TextDocumentEdit` object
---@param index number: Optional index of the edit, if from a list of edits (or nil, if not from a list)
---@see https://microsoft.github.io/language-server-protocol/specifications/specification-current/#textDocumentEdit
function M.apply_text_document_edit(text_document_edit, index)
  local text_document = text_document_edit.textDocument
  local bufnr = vim.uri_to_bufnr(text_document.uri)

  -- For lists of text document edits,
  -- do not check the version after the first edit.
  local should_check_version = true
  if index and index > 1 then
    should_check_version = false
  end

  -- `VersionedTextDocumentIdentifier`s version may be null
  --  https://microsoft.github.io/language-server-protocol/specification#versionedTextDocumentIdentifier
  if should_check_version and (text_document.version
      and text_document.version > 0
      and util.buf_versions[bufnr]
      and util.buf_versions[bufnr] > text_document.version) then
    print("Buffer ", text_document.uri, " newer than edits.")
    return
  end

  return M.apply_text_edits(text_document_edit.edits, bufnr)
end

local function create_file(change)
  local opts = change.options or {}
  -- from spec: Overwrite wins over `ignoreIfExists`
  local fname = vim.uri_to_fname(change.uri)
  if not opts.ignoreIfExists or opts.overwrite then
    local file = io.open(fname, 'w')
    file:close()
  end
  vim.fn.bufadd(fname)
end

local function delete_file(change)
  local opts = change.options or {}
  local fname = vim.uri_to_fname(change.uri)
  local stat = vim.loop.fs_stat(fname)
  if opts.ignoreIfNotExists and not stat then
    return
  end
  assert(stat, "Cannot delete not existing file or folder " .. fname)
  local flags
  if stat and stat.type == 'directory' then
    flags = opts.recursive and 'rf' or 'd'
  else
    flags = ''
  end
  local bufnr = vim.fn.bufadd(fname)
  local result = tonumber(vim.fn.delete(fname, flags))
  assert(result == 0, 'Could not delete file: ' .. fname .. ', stat: ' .. vim.inspect(stat))
  api.nvim_buf_delete(bufnr, { force = true })
end


--- Applies a `WorkspaceEdit`.
---
--@param workspace_edit (table) `WorkspaceEdit`
-- @see https://microsoft.github.io/language-server-protocol/specifications/specification-current/#workspace_applyEdit
function M.apply_workspace_edit(workspace_edit)
  local tab_stop = nil

  if workspace_edit.documentChanges then
    for idx, change in ipairs(workspace_edit.documentChanges) do
      if change.kind == "rename" then
        util.rename(
          vim.uri_to_fname(change.oldUri),
          vim.uri_to_fname(change.newUri),
          change.options
        )
      elseif change.kind == 'create' then
        create_file(change)
      elseif change.kind == 'delete' then
        delete_file(change)
      elseif change.kind then
        error(string.format("Unsupported change: %q", vim.inspect(change)))
      else
        local tab_stop_loc = M.apply_text_document_edit(change, idx)
        if tab_stop_loc ~= nil then
            tab_stop = {
                uri = change.textDocument.uri,
                range = {start = tab_stop_loc, ["end"] = tab_stop_loc}
            }
        end
      end
    end
  end

  local all_changes = workspace_edit.changes
  if not (all_changes and not vim.tbl_isempty(all_changes)) then
    return tab_stop
  end

  for uri, changes in pairs(all_changes) do
    local bufnr = vim.uri_to_bufnr(uri)
    local tab_stop_loc = M.apply_text_edits(changes, bufnr)
    if tab_stop_loc ~= nil then
        tab_stop = {
            uri = uri,
            range = {start = tab_stop_loc, ["end"] = tab_stop_loc}
        }
    end
  end
  return tab_stop

end

function M.location_link_to_item(location_link, text)
	local uri = location_link.targetUri
	if uri and string.sub(uri,1,7) == "file://" then
	   return {
	   	text = text,
	   	col = get_line_byte_from_position(0,location_link.targetRange.start)+1,
	   	lnum = location_link.targetRange.start.line + 1,
	   	filename = string.sub(uri,8),
	   	}
	else
	   return {
	   	text = text,
	   	}
	end
end

function M.for_all_rust_buffers(f)
  for _,buf in ipairs(vim.api.nvim_list_bufs()) do
    for _,client in ipairs(vim.lsp.buf_get_clients(buf)) do
      if client.name == "rust_analyzer" then
        f(buf)
      end
    end
  end
end
function M.buf_get_rust_analyzer(buffer)
  if not buffer then
    buffer = 0
  end
  for _,client in ipairs(vim.lsp.buf_get_clients(buffer)) do
    if client.name == "rust_analyzer" then
      return client
    end
  end
  return nil
end

do
  local registered_sch = {}
  local scheduled = {}

  local function add_au(buffer)
      vim.cmd(string.format([[
        augroup i_love_rust_semantic_reset_schedule
        au! * <buffer=%d>
        au InsertLeave <buffer=%d> ++once lua require"i-love-rust.utils"._execute_sch_callbacks(%d)
        augroup END
      ]],buffer,buffer,buffer))
  end

  local function remove_au(buffer)
      vim.cmd(string.format([[
        augroup i_love_rust_semantic_reset_schedule
        au! * <buffer=%d>
        augroup END
      ]],buffer))
  end

  local function in_insert()
    local mod = vim.api.nvim_get_mode()
    return string.sub(mod.mode,1,1) =='i'
  end

  local ticks = {}
  local function set_dirty(_,buffer,tick)
    ticks[buffer]=tick
    for _,callback in pairs(registered_sch[buffer]) do
      callback.clean = false
    end
    
    if not scheduled[buffer] then
      scheduled[buffer] = true;
      vim.schedule(function() M._execute_sch_callbacks(buffer) end)
    end
  end

  local function register(buffer)
    if not registered_sch[buffer] then
      registered_sch[buffer]={}
      api.nvim_buf_attach(buffer,false,
      {
        on_lines = set_dirty
      })
    end
  end

  function M._execute_sch_callbacks(buffer)
    for id,sch in pairs(registered_sch[buffer]) do
      if not in_insert() then
        sch.clean=true
        sch.callback()
      else
        add_au(buffer)
        return
      end
    end
    scheduled[buffer]=nil;
  end

  local function register_callback(buffer,id,fun)
      register(buffer)
      registered_sch[buffer][id]={callback=fun,clean=false}
  end

  local function fixbuffer(buffer)
    if buffer == 0 then
      buffer = api.nvim_get_current_buf()
    end
    return buffer
  end

  function M.register_txt_change_callback(buffer,id,fun)
    buffer = fixbuffer(buffer)

    register_callback(buffer,id,fun)

    if not in_insert() then
      registered_sch[buffer][id].clean=true
      fun()
    else
      add_au(buffer)
    end
  end

  function M.remove_txt_change_callback(buffer,id)
    buffer = fixbuffer(buffer)
    registered_sch[buffer][id]=nil
  end
  function M.callback_is_clean(buffer,id)
    buffer = fixbuffer(buffer)
    return registered_sch[buffer][id].clean
  end
  function M.callback_set_dirty(buffer,id)
    buffer = fixbuffer(buffer)
    if not in_insert() then
      registered_sch[buffer][id].clean=true
      registered_sch[buffer][id].callback()
    else
      registered_sch[buffer][id].clean = false
      add_au(buffer)
    end
  end
  function M.get_tic(buffer)
    register(buffer)
    buffer = fixbuffer(buffer)
    return ticks[buffer] or 0
  end
end

function M.buf_get_associated_workspace_dirs(buffer)
  local rust_analyzer = M.buf_get_rust_analyzer(buffer)
  local dirs = {}
  for _,dir in ipairs(rust_analyzer.workspaceFolders or {}) do
    table.insert(dirs, dir.name)
  end
  return dirs
end

function M.with_package_list(workspace_root_dir,f,package_filter)
    local job = require('plenary.job')
    job:new({
        command = "cargo",
        args={"metadata","--no-deps","--format-version","1"},
        cwd = workspace_root_dir,
        on_exit = function(j,code)
          if code ~= 0 then
            vim.notify("failed to retrive package metadata")
            return
          end
          vim.inspect("meta result",j)
          vim.schedule(function()
            local result = j:result()
            if not result or #result == 0 then
              vim.notify("failed to retrive package metadata")
              return
            end
            local status,json = pcall(vim.fn.json_decode,result[1])
            if status == false then
              vim.notify("failed to parse `cargo metadata` result into json")
              return
            end
            local list = {}
            for _,package in ipairs(json.packages or {}) do
              local id = package.id
              local path = id:gsub(".*%(path%+file://",""):sub(1,-2)
              table.insert(list,path)
            end
            f(list)
          end)
        end}
   ):start()
end

function M.with_package_source_file_list(package_dir,f)
    local job = require('plenary.job')
    local is_windows = uv.os_uname().version:match'Windows'
    local path_sep = is_windows and '\\' or '/'
    job:new({
        command = "cargo",
        args = {"package","-l","--no-verify","--allow-dirty"},
        cwd = package_dir,
        on_exit = function(j,_)
          vim.schedule(function ()
            local file_list = {}
            for _,file in ipairs(j:result()) do
              if file:len()>=3 and file:sub(-3) == ".rs" then
                table.insert(file_list, package_dir..path_sep..file)
              end
            end
            f(file_list)
          end)
        end
      }):start()
end

function M.with_current_workspace_source_file_list(buffer,f,package_filter)
  local dirs = M.buf_get_associated_workspace_dirs(buffer)
  local cur_dir = 0
  if #dirs == 0 then
    return
  end
  local file_list = {}
  local function recurs_dir()
    cur_dir = cur_dir + 1
    M.with_package_list(dirs[cur_dir], function(package_list)
      local cur_package = 0
      local function recurs_package()
        while cur_package ~= #package_list do
          cur_package = cur_package+1
          local package= package_list[cur_package]
          if not package_filter or package_filter(package) then
            M.with_package_source_file_list(package, function(list)
              --vim.tbl_extend cause trouble here
              for _,v in ipairs(list) do
                table.insert(file_list, v)
              end

              if cur_package == #package_list then
                if cur_dir == #dirs then
                  f(file_list)
                else
                  recurs_dir()
                end
                return
              end
              recurs_package()
            end)
            return
          end
        end
        if cur_dir == #dirs then
          f(file_list)
          return
        end
        recurs_dir()
        end
        recurs_package()
      end
    )
  end
  recurs_dir()
end


return M
