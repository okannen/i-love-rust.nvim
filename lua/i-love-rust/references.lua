local M = {}
local lsp = vim.lsp
local util = lsp.util
local ui = require'i-love-rust.ui'
local manage_error = require'i-love-rust.error'.manage_error


function M.locations_answer_handler(e,res)
  if e then
    manage_error(e)
    return
  end
  if res == nil then
    return
  end
  if res.uri then
    res = {res}
  end
  if #res == 1 then
    util.jump_to_location(res[1])
  else
    ui.locations_view(res)
  end
end

local function make_call_handler(direction)
  return function(_, result)
    if not result then return end
    local items = {}
    for _, call_hierarchy_call in pairs(result) do
      local call_hierarchy_item = call_hierarchy_call[direction]
      for _, range in pairs(call_hierarchy_call.fromRanges) do
        table.insert(items, {
          uri = call_hierarchy_item.uri,
          range = range
        })
      end
    end
    ui.locations_view(items)
  end
end

M.outgoing_call_handler = make_call_handler('to')
M.incoming_call_handler = make_call_handler('from')

function M.locations_of(loc_type)
  local param = util.make_position_params()
  lsp.buf_request(0,"textDocument/"..loc_type,param, M.locations_answer_handler)
end
function M.references()
  local param = util.make_position_params()
  param.context = {includeDeclaration=false}
  lsp.buf_request(0,"textDocument/references",param, M.locations_answer_handler)
end


return M

