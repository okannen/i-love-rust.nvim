local ra_config = require 'i-love-rust.config'

local M = {}

function M.start_standalone_client()
    local config = {
        root_dir = require('lspconfig.util').path.dirname(
            vim.api.nvim_buf_get_name(0)),
        capabilities = ra_config.options.server.capabilities,
        cmd = {'rust-analyzer'},
        init_options = {detachedFiles = {vim.api.nvim_buf_get_name(0)}},
        on_init = function(client)
            vim.lsp.buf_attach_client(0, client.id)
            vim.cmd "command! RustSetInlayHints :lua require('i-love-rust.inlay_hints').set_inlay_hints()"
            vim.cmd "command! RustDisableInlayHints :lua require('i-love-rust.inlay_hints').disable_inlay_hints()"
            vim.cmd "command! RustToggleInlayHints :lua require('i-love-rust.inlay_hints').toggle_inlay_hints()"
            vim.cmd "command! RustExpandMacro :lua require('i-love-rust.expand_macro').expand_macro()"
            vim.cmd "command! RustJoinLines :lua require('i-love-rust.join_lines').join_lines()"
            vim.cmd "command! -range RustHoverActions :lua require('i-love-rust.hover_actions').hover_actions()"
            vim.cmd "command! RustMoveItemDown :lua require('i-love-rust.move_item').move_item()"
            vim.cmd "command! RustMoveItemUp :lua require('i-love-rust.move_item').move_item(true)"
            vim.cmd "command! -nargs=* RustRunSingle :lua require('i-love-rust.codelens').run('rust-analyzer.runSingle',{<f-args>})"
            vim.cmd "command! -nargs=* RustDebugSingle :lua require('i-love-rust.codelens').run('rust-analyzer.debugSingle',{<f-args>})"
            vim.cmd "command! RustSetCodeLens :lua require('i-love-rust.codelens').refresh()"
        end,
        on_exit = function()
            vim.cmd "delcommand RustSetInlayHints"
            vim.cmd "delcommand RustDisableInlayHints"
            vim.cmd "delcommand RustToggleInlayHints"
            vim.cmd "delcommand RustExpandMacro"
            vim.cmd "delcommand RustJoinLines"
            vim.cmd "delcommand RustHoverActions"
            vim.cmd "delcommand RustMoveItemDown"
            vim.cmd "delcommand RustMoveItemUp"
            vim.cmd "delcommand RustRunSingle"
            vim.cmd "delcommand RustDebugSingle"
            vim.cmd "delcommand RustSetCodeLens"
        end,
        handlers = ra_config.options.server.handlers,
    }

    if ra_config.options.tools.autoSetHints then
        require'i-love-rust.inlay_hints'.setup_autocmd()
    end

    if ra_config.options.tools.autoSetCodeLenses then
        require'i-love-rust.codelens'.setup_autocmd()
    end
    require'i-love-rust.codelens'.refresh()

    require'i-love-rust.inlay_hints'.set_inlay_hints()

    vim.lsp.start_client(config)
    vim.lsp.handlers["textDocument/publishDiagnostics"] = vim.lsp.with(
      vim.lsp.diagnostic.on_publish_diagnostics, {
        virtual_text = true,
        signs = true,
        update_in_insert = true,
      }
     )
end

return M
