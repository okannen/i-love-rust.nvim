-- ?? helps with all the warnings spamRelatAbsNumber
local util = vim.lsp.util
local config = require('i-love-rust.config')
local code_lens = require'i-love-rust.codelens'
local ui = require'i-love-rust.ui'
local rutils = require'i-love-rust.utils'
local act = require'i-love-rust.actions'
local manage_error = require'i-love-rust.error'.manage_error
local api = vim.api

local M = {}

function M.on_enter()
    local loc = util.make_position_params()
    vim.lsp.buf_request(0, "experimental/onEnter", loc,function(err,text_edit)
        assert(err == nil, vim.inspect(err))
        assert(text_edit ~= nil, "not yet implemented by rust-analyzer")
        assert(#text_edit == 1, "Surprise: more than one text edit for a cariage return!")
	act.edit(text_edit)
    end
    )
end

function M.open_window(name, side, cur_win_id)

    return function(_,result)

      if not side and ui.exist(name) then
         ui.focus(name)
         return
      end

      local actions = result and result.actions
      local text = result and result.text

      local text_lines = text and util.convert_input_to_markdown_lines(text) or {}
      text_lines = util.trim_empty_lines(text_lines)

      if not side then
        local focus = config.options.tools.hover_actions.auto_focus
        ui.open_floating_window(name, text_lines, act.commands_to_callbacks(actions), focus)
      else
        ui.open_side_window(name, text_lines, act.commands_to_callbacks(actions), false, cur_win_id)
      end
    end
end

function M.execute_single_action_or_open_selection(kind,side)
  return function(b,result)
     if not result.actions then
       return
     end
     local coms = act.commands_to_callbacks(result.actions)
     if #coms == 1 then
       coms[1].callback()
     else
       M.open_window(kind, side)(b,result)
     end
  end
end


local function with_code_action_range(f, range, only)
    return function(bufnr,result)
        local context = { diagnostics = vim.lsp.diagnostic.get_line_diagnostics(), only = only }
        local params = range
        params.context = context
        vim.lsp.buf_request(bufnr, "textDocument/codeAction", params,
            function(e,actions)
		            if e then
		              manage_error(e)
		              return
		            end
                --print("actions",vim.inspect(actions))

                result = result or {}

                if not result.actions then
                    result.actions = actions;
                else
                    vim.list_extend(result.actions, actions)
                end

                f(bufnr,result)
            end
        )
    end
end

function M.with_code_action(f, only)
    return with_code_action_range(f, util.make_range_params(), only)
end

function M.with_range_code_action(f, only)
    local params = rutils.make_given_range_params()
    return with_code_action_range(f, params, only)
end

function M.with_code_lens_actions(f)
    return function(bufnr,result)
                result = result or {}
                if not result.actions then
                  result.actions={}
                end
                code_lens.insert_curline_lens_commands(result.actions)
                f(bufnr,result)
            end
end

local function with_hover_pos_range(f,only_actions,params)
    return function(bufnr,result)
        vim.lsp.buf_request(bufnr, "textDocument/hover", params,
            function(e,hres)
		          if e then
		            manage_error(e)
		            return
		          end
             -- print("hres",vim.inspect(hres))

		          if hres then
                          	local content = (not only_actions) and hres.contents or nil
                          	result = result or {}
                          	for _,a in pairs(type(hres) == "table" and hres.actions or {}) do
                          	  if not result.actions then
                          	      result.actions = a.commands;
                          	  else
                          	      vim.list_extend(result.actions, a.commands)
                          	  end
                          	end
                          	result.text = content
		          end

                          f(bufnr,result)
            end
          )
       end
end

function M.with_hover(f,only_actions)
    return with_hover_pos_range(f, only_actions, vim.lsp.util.make_position_params())
end

function M.with_hover_range(f,only_actions)
    local param = rutils.make_given_range_params()
    param.position = param.range
    param.range = nil
    return with_hover_pos_range(f, only_actions, param)
end

function M.range_code_action()
    M.with_range_code_action(M.open_window("code actions",true,api.nvim_get_current_win()))(0)
end

-- Sends the request to rust-analyzer to get hover actions and handle it
function M.actions()
    local side = not config.options.tools.hover_actions.float_code_actions
    if rutils.is_range_selection_mode() then
      M.with_range_code_action(M.open_window("code actions",side,api.nvim_get_current_win()))(0)
    else
      M.with_code_lens_actions(M.with_code_action(M.open_window("code actions", side,api.nvim_get_current_win())))(0)
    end
end

function M.quickfix()
    if rutils.is_range_selection_mode() then
      M.with_range_code_action(M.execute_single_action_or_open_selection("quickfix"),{"quickfix"})(0)
    else
      M.with_code_action(M.execute_single_action_or_open_selection("quickfix"),{"quickfix"})(0)
    end
end

function M.hover()
    local side = not config.options.tools.hover_actions.float_hover
    if rutils.is_range_selection_mode() then
       M.with_hover_range(M.open_window("hover", side,api.nvim_get_current_win()))(0)
    else
       M.with_hover(M.open_window("hover", side,api.nvim_get_current_win()))(0)
    end
end

function M.set_hover_style(float)
    config.options.tools.hover_actions.float_hover = float
    if float then
      vim.cmd("augroup i_love_rust_hover")
      vim.cmd("au!")
      vim.cmd("augroup END")
      ui.close_window("hover")
    else
      vim.cmd("augroup i_love_rust_hover")
      vim.cmd("au!")
      rutils.for_all_rust_buffers(function(buf)
	       vim.cmd("autocmd CursorMoved,BufWritePost,InsertLeave <buffer="..buf.."> lua require'i-love-rust.hover_actions'.hover()")
       end)
      vim.cmd("augroup END")
      M.hover()
    end
end

function M.switch_hover_style()
    local cond = not config.options.tools.hover_actions.float_hover
    M.set_hover_style(cond)
end

function M.set_code_actions_style(float)
    config.options.tools.hover_actions.float_code_actions = float
    if float then
      vim.cmd("augroup i_love_rust_code_actions")
      vim.cmd("au!")
      vim.cmd("augroup END")
      ui.close_window("code actions")
    else
      vim.cmd("augroup i_love_rust_code_actions")
      vim.cmd("au!")
      rutils.for_all_rust_buffers(function(buf)
	       vim.cmd("autocmd CursorMoved,BufWritePost,InsertLeave <buffer="..buf.."> lua require'i-love-rust.hover_actions'.actions()")
       end)
      vim.cmd("augroup END")
      M.actions()
    end
end

function M.switch_code_actions_style()
    local cond = not config.options.tools.hover_actions.float_code_actions
    M.set_code_actions_style(cond)
end

function M.set_up(buffer)
  if not config.options.tools.hover_actions.float_code_actions then
      vim.cmd("augroup i_love_rust_code_actions")
	    vim.cmd("autocmd CursorMoved,InsertLeave,TextChanged <buffer="..buffer.."> lua require'i-love-rust.hover_actions'.actions()")
      vim.cmd("augroup END")
  end
  if not config.options.tools.hover_actions.float_hover then
      vim.cmd("augroup i_love_rust_hover")
	    vim.cmd("autocmd CursorMoved,BufWritePost,InsertLeave,TextChanged <buffer="..buffer.."> lua require'i-love-rust.hover_actions'.hover()")
      vim.cmd("augroup END")
  end
end

function M.on_code_action_key()
  local count = vim.v.count
  if count ~= 0 then
    vim.cmd('exe "normal! \\<Esc>"')
    ui.run_command("code actions", count)
  else
    M.actions()
  end
end

function M.on_hover_key()
  local count = vim.v.count
  if count ~= 0 then
    vim.cmd('exe "normal! \\<Esc>"')
    ui.run_command("hover", count)
  else
    M.hover()
  end
end

function M.on_quickfix_key()
  local count = vim.v.count
  if count ~= 0 then
    vim.cmd('exe "normal! \\<Esc>"')
    ui.run_command("quickfix", count)
  else
    M.quickfix()
  end
end


return M
