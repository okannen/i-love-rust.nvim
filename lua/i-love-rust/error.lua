
local M = {}

M.error_codes = {
	MethodNotFound = -32601
}

M.debug_mode = false

function M.manage_error(e)
   if e == nil then
      return
   elseif e.code == M.error_codes.MethodNotFound then
      -- the server is likely not started yet
      if M.debug_mode then
        print(vim.inspect(e))
      end
   else
      vim.notify("i-love-rust, unexpected LSP error (" .. string.format("%d",e.code).. "): " .. e.message)
   end
end

return M
