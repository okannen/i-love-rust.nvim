local vim = vim
local nvim_lsp = require 'lspconfig'
local config = require 'i-love-rust.config'
local rutils = require('i-love-rust.utils')
local lspconfig_utils = require('lspconfig.util')
local rt_dap = require('i-love-rust.dap')

local M = {}

local function setupCommands()
    local lsp_opts = config.options.server

    lsp_opts.commands = vim.tbl_deep_extend("force", lsp_opts.commands or {}, {
        RustSetInlayHints = {
            function()
                require('i-love-rust.inlay_hints').set_inlay_hints()
            end
        },
        RustDisableInlayHints = {
            require('i-love-rust.inlay_hints').disable_inlay_hints
        },
        RustToggleInlayHints = {
            function()
                require('i-love-rust.inlay_hints').toggle_inlay_hints()
            end
        },
        RustExpandMacro = {require('i-love-rust.expand_macro').expand_macro},
        RustOpenCargo = {require('i-love-rust.open_cargo_toml').open_cargo_toml},
        RustParentModule = {require('i-love-rust.parent_module').parent_module},
        RustJoinLines = {require('i-love-rust.join_lines').join_lines},
        RustRunnables = {
            function() require('i-love-rust.runnables').runnables() end
        },
        RustActions = {require('i-love-rust.hover_actions').actions},
        RustHover = {require('i-love-rust.hover_actions').hover},
        RustMoveItemDown = {
            function() require('i-love-rust.move_item').move_item() end
        },
        RustMoveItemUp = {
            function()
                require('i-love-rust.move_item').move_item(true)
            end
        },
        RustRunSingle = {
            function(...)
                require('i-love-rust.codelens').run("rust-analyzer.runSingle", {...})
            end,
            ["nargs=*"]=true
        },
        RustDebugSingle = {
            function(...)
                require('i-love-rust.codelens').run("rust-analyzer.debugSingle", {...})
            end,
            ["nargs=*"]=true
        },
        RustRelatedTests = {require('i-love-rust.runnables').related_tests},
        RustSetCodeLenses = {
            function()
                require('i-love-rust.codelens').refresh()
            end
        },
        RustReloadWorkspace = {
            function()
                vim.lsp.buf_request(0, "rust-analyzer/reloadWorkspace")
            end
        },
        RustCrateGraph = {
            function(full)
                require('i-love-rust.crate_graph').crate_graph(full~=nil)
            end,
            ["nargs=?"]=true
        },
        RustQuickFix = {require('i-love-rust.hover_actions').quickfix},
	      RustOpenExternalDoc = {require'i-love-rust.external_documentation'.external_docs},
        RustSwitchHoverStyle = {require'i-love-rust.hover_actions'.switch_hover_style},
        RustSwitchCodeActionsStyle = {require'i-love-rust.hover_actions'.switch_code_actions_style},
        RustKeyMap = {require'i-love-rust.config'.show_key_map},
        RustFormat = {vim.lsp.buf.formatting},
        RustListUnsafe = {
            function(workspace)
                require('i-love-rust.audit_unsafe').list_unsafe(workspace~=nil)
            end,
            ["nargs=*"]=true
        },
    })
end

local function setup_handlers()
    local lsp_opts = config.options.server
    local tool_opts = config.options.tools
    local custom_handlers = {}

    if tool_opts.hover_with_actions == nil then
        tool_opts.hover_with_actions = true
    end
    if tool_opts.hover_with_actions then
        custom_handlers["textDocument/hover"] =
            require('i-love-rust.hover_actions').handler
    end
    custom_handlers["textDocument/codeLens"] =
        require('i-love-rust.codelens').on_codelens

    custom_handlers["experimental/serverStatus"] =
        require('i-love-rust.server_status').on_notification

    custom_handlers["callHierarchy/incomingCalls"] =
        require('i-love-rust.references').incoming_call_handler

    custom_handlers["callHierarchy/outgoingCalls"] =
        require('i-love-rust.references').outgoing_call_handler

    custom_handlers["textDocument/declaration"] =
        require('i-love-rust.references').location_answer_handler

    custom_handlers["textDocument/definition"] =
        require('i-love-rust.references').location_answer_handler

    custom_handlers["textDocument/typeDefinition"] =
        require('i-love-rust.references').location_answer_handler

    custom_handlers["textDocument/implementation"] =
        require('i-love-rust.references').location_answer_handler

    custom_handlers["textDocument/references"] =
        require('i-love-rust.references').location_answer_handler

    -- PB: rust-analyzer does not follow the spec
    -- and send it at every character insert!
    --custom_handlers["workspace/semanticTokens/refresh"] =
    --    require('i-love-rust.semantic').on_notification

    lsp_opts.handlers = vim.tbl_deep_extend("force", custom_handlers,
                                            lsp_opts.handlers or {})

end

local function setup_capabilities()
    local lsp_opts = config.options.server
    local capabilities = vim.lsp.protocol.make_client_capabilities()

    --capabilities.offsetEncoding = {'utf-8','utf-16'}

    capabilities.textDocument.selectionRange = {dynamicRegistration = false}
    capabilities.textDocument.foldingRange = {dynamicRegistration = false, lineFoldingOnly=true}
    capabilities.textDocument.semanticTokens = {
      dynamicRegistration = false,
      requests = {
        range = true,
        full = { delta = true},
      },
      tokenTypes = {""},
      tokenModifiers = {""},
      formats = {"relative"},
      overlappingTokenSupport = true,
      multilineTokenSupport = false,
    }


    -- send actions with hover request
    capabilities.experimental = {
        hoverActions = true,
        hoverRange = true,
        serverStatusNotification = true,
        snippetTextEdit = true,
        codeActionGroup = true,
    }

    -- rust analyzer goodies
    capabilities.experimental.commands =
        {
            commands = {
                "rust-analyzer.runSingle", "rust-analyzer.debugSingle",
                "rust-analyzer.showReferences", "rust-analyzer.gotoLocation",
                "editor.action.triggerParameterHints"
            }
        }

    -- setup completion caps
    local has_cmp,cmp_nvim = pcall(require,'cmp_nvim_lsp')
    if has_cmp then
      capabilities = cmp_nvim.update_capabilities(capabilities)
    end

    --capabilities.workspace = {
    --  semanticTokens = {
    --    refreshSupport = true
    --  },
    --}


    lsp_opts.capabilities = vim.tbl_deep_extend("force", capabilities,
                                                lsp_opts.capabilities or {})

end

local function setup_lsp() nvim_lsp.rust_analyzer.setup(config.options.server) end

local function get_root_dir()
    local fname = vim.api.nvim_buf_get_name(0)
    local cargo_crate_dir = lspconfig_utils.root_pattern 'Cargo.toml'(fname)
    local cmd = 'cargo metadata --no-deps --format-version 1'
    if cargo_crate_dir ~= nil then
        cmd = cmd .. ' --manifest-path ' ..
                  lspconfig_utils.path.join(cargo_crate_dir, 'Cargo.toml')
    end
    local cargo_metadata = vim.fn.system(cmd)
    local cargo_workspace_dir = nil
    if vim.v.shell_error == 0 then
        cargo_workspace_dir =
            vim.fn.json_decode(cargo_metadata)['workspace_root']
    end
    return cargo_workspace_dir or cargo_crate_dir or
               lspconfig_utils.root_pattern 'rust-project.json'(fname) or
               lspconfig_utils.find_git_ancestor(fname)
end

function M.setup(opts)

    require"i-love-rust.highlighting".setup()

    config.setup(opts)

    require"i-love-rust.keymap".setup_global_mapping(opts.tools.keymaps)

    setup_capabilities()
    -- setup handlers
    setup_handlers()
    -- setup user commands
    setupCommands()
    -- setup rust analyzer
    setup_lsp()

    if rutils.is_bufnr_rust(0) and (get_root_dir() == nil) then
        require('i-love-rust.standalone').start_standalone_client()
    end

    if pcall(require, 'dap') then rt_dap.setup_adapter() end
    
    if opts.tools.setup_document_highlight then
      vim.cmd([[
      autocmd CursorHold * lua vim.lsp.buf.document_highlight()
      autocmd CursorHoldI * lua vim.lsp.buf.document_highlight()
      autocmd CursorMoved * lua vim.lsp.buf.clear_references()
      ]])
    end

end

return M
