command RustStartStandaloneServerForBuffer :lua require('i-love-rust.standalone').start_standalone_client()

hi link InlayHints Comment
hi link LSPCodeLens InlayHints

if has('macunix')
	let g:i_love_rust_open_cmd = get(g:,"i_love_rust_open_cmd","open")
elseif has('unix')
	let g:i_love_rust_open_cmd = get(g:,"i_love_rust_open_cmd","xdg-open")
elseif has('win32')
	let g:i_love_rust_open_cmd = get(g:,"i_love_rust_open_cmd","start")
end

command -nargs=* Run :RustRunSingle <args>
command -nargs=* Debug :RustDebugSingle <args>
command CargoToml :RustOpenCargo

function SearchReplace(query) range
		:call luaeval("require'i-love-rust.search_replace'.search_replace_lines(_A[1],_A[2],_A[3])",[a:query,a:firstline, a:lastline])
endfunction
function WorkspaceSearchReplace(query)
		:call luaeval("require'i-love-rust.search_replace'.search_replace(_A)",a:query)
endfunction

command -nargs=* -range RustSearchReplace <line1>,<line2>call SearchReplace(<q-args>)
command -nargs=* RustWorkspaceSearchReplace call WorkspaceSearchReplace(<q-args>)
