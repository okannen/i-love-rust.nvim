# i-love-rust.nvim

Add code diagnostics, semantic higlighting and the full set of [rust-analyzer](https://rust-analyzer.github.io/manual.html#features) support
when edditing rust code.

![Intro-image](https://gitlab.com/api/v4/projects/okannen%2Fdoc_pictures/repository/files/i-love-rust.nvim%2Fintro.png/raw?ref=master)

## Installation

### Install rust-analyzer 

Install rust analyzer with [rustup](https://www.rust-lang.org/tools/install). On the command line enter:

```
$ rustup +nightly component add rust-analyzer-preview
```

Check the version by running:

```
rust-analyzer --version
```

A date should appear. In case of error or if rust-analyzer is out-dated:

  1. Check that rustup binary dir are in path. To fix that you could add
the following line to your `~/.bashrc` file:
    ```
    export PATH=~/.cargo/bin:~/.rustup/toolchains/stable-x86_64-unknown-linux-gnu/bin:~/.rustup/toolchains/nightly-x86_64-unknown-linux-gnu/bin:$PATH
    ```
  2. Update rust by running on the command line `rustup update`

### Modify the neovim init script

The initialization script path may be `~/.config/nvim/init.vim`.

Supposing that your pluggin manager is [vim-plug](https://github.com/junegunn/vim-plug), 
and that you installed it in `~/.local/share/nvim` your init script may look like this:
```
call plug#begin('~/.local/share/nvim/plugged')

Plug 'neovim/nvim-lspconfig'

let g:i_love_rust_key_bindings = 1 
Plug 'https://gitlab.com/okannen/i-love-rust.nvim'

Plug 'nvim-lua/popup.nvim'

Plug 'nvim-lua/plenary.nvim'

Plug 'nvim-telescope/telescope.nvim'

Plug 'mfussenegger/nvim-dap'

call plug#end('~/.local/share/nvim/plugged')

"optionaly:
  "swap were usefull before SSD
  set noswapfile
  "supposedly good
  if &term != "linux" then
    set termguicolors
  endif

lua require"i-love-rust".setup(
  { 
  tools = {
    keymaps = {
      [" a"] = "code actions",
      [" d"] = "declaration",
      [" D"] = "definition",
      [" e"] = "line diagnostics",
      [" h"] = "hover",
      [" H"] = "open external doc",
      [" i"] = "incoming calls",
      [" J"] = "join lines",
      [" m"] = "implementation",
      [" o"] = "outgoing calls",
      [" p"] = "parent module",
      [" ql"] = "diagnostic locations",
      [" qL"] = "workspace diagnostic locations",
      [" qe"] = "error locations",
      [" qf"] = "quickfix",
      [" qu"] = "list unsafe",
      [" qU"] = "list worskpace unsafe",
      [" rd"] = "debug",
      [" re"] = "references",
      [" rt"] = "related tests",
      [" ru"] = "run",
      [" rn"] = "rename",
      [" s"] = "signature help",
      ["<c-s>"] = "insert mode signature help",
      [" w"] = "select containing",
      [" W"] = "undo select containing",
      [" y"] = "token semantic",
      [" z"] = "fold",
      ["<F8>"] = "debugger scopes",
      ["<F9>"] = "debugger frames",
      ["<F10>"] = "debugger terminal",
      [" b"] = "debugger toggle breakpoint",
      [" B"] = "debugger n-hits breakpoint",
      [" c"] = "debugger continue",
      [" j"] = "debugger step over",
      [" J"] = "debugger stacktrace up",
      [" g"] = "debugger run to cursor",
      [" k"] = "debugger step out",
      [" K"] = "debugger stacktrace down",
      [" l"] = "debugger step into",
      [" v"] = "debugger expression",
      ["<C-k>"] = "move item up",
      ["<C-j>"] = "move item down",
      }
      }
  })
```
By default, it does not provide keymaps. You should define your own.

On the left side of `=` appears a key sequence, that may be triggered
in normal mode and potentially too in visual mode and on the right a 
functionnality provided by `i-love-rust`. See bellow for further details.

Some of those key mapping will be applied globaly, or only on buffer
attached to rust-analyzer if only rust-analyzer has such functionality.

### Install completion support
`i-love-rust` does not provide completion support but it will setup server
capabilities approprietly if it detects the presence of `hrsh7th` pluggins.

Add hrsh7th pluggins to get completion support, `i-love-rust` will
setup it correctly.

Add the following lines to the initialization script (considering you use plugged)
between after `plug#begin` and `plug#end`:
```vim
Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'hrsh7th/cmp-path'
Plug 'hrsh7th/cmp-buffer'
Plug 'hrsh7th/nvim-cmp'
Plug 'hrsh7th/cmp-vsnip'
Plug 'hrsh7th/vim-vsnip'

```
Then add the following lines at the end of the initialization script. If you are
not a touch-typist you may which to set `autocomplete=true`. Otherwise hit `control + space` to see the completion popup.
```vim
lua <<EOF
local cmp = require'cmp'
cmp.setup({
  -- Enable LSP snippets
  snippet = {
    expand = function(args)
        vim.fn["vsnip#anonymous"](args.body)
    end,
  },
  mapping = {
    ['<C-p>'] = cmp.mapping.select_prev_item(),
    ['<C-n>'] = cmp.mapping.select_next_item(),
    -- Add tab support
    --['<S-Tab>'] = cmp.mapping.select_prev_item(),
    --['<Tab>'] = cmp.mapping.select_next_item(),
    ['<C-d>'] = cmp.mapping.scroll_docs(-4),
    ['<C-f>'] = cmp.mapping.scroll_docs(4),
    ['<C-Space>'] = cmp.mapping.complete(),
    ['<C-e>'] = cmp.mapping.close(),
    ['<CR>'] = cmp.mapping.confirm({
      behavior = cmp.ConfirmBehavior.Insert,
      select = true,
    })
  },

  -- Installed sources
  sources = {
    { name = 'nvim_lsp' },
    { name = 'vsnip' },
    { name = 'path' },
    { name = 'buffer' },
  },
  preselect = false,
  completion = {
    autocomplete = false,
  }
})
EOF
```

### Other thing you may need

  Setup a font in your terminal that supports icons (nerd-fonts), italic and bold style

# Adjusting settings

In the initialization script, the function `require'i-love-rust'.setup()` can take
an option to adjust settings. Calling `require'i-love-rust'.setup()` in the initialization
script is equivalent to:
```vim
lua<<EOF
local opts = {
    tools = { -- i-love-rust options

        -- maps keymap => functionnality
        -- see README.md
        keymaps = {},

        -- show function signature in hover when the cursor is on '(' or after it in insert mode
        -- and show parameter name after or on ',' character
        auto_signature_help = true,

        --- highlight all occurence of the entity under the cursor
        setup_document_highlight = true,

        -- if not nil, use rust-analyzer semantic to highlight code
        highlight = {
            -- function for highliting, default to require"i-love-rust".highlighting.simple_highlighter
            --  param: token
            --    token.type => string, token type
            --    token.modifiers => list of token modifiers
            --    token.modifiers:contains(modifier) => return true if modifier in modifiers list
            --    token:highlight(group) => apply higlight group to token
            --    token:get_text() => return the token string
            --  exemple => see `simple_highlighter` in module "lua/i-love-rust/highlighting.lua" 
            --  to see list of types and modifiers, run the command
            --    :lua print(vim.inspect(require"i-love-rust.semantic".legend))
            token_highlighter = nil,

            -- shall vim syntax be kept
            overlay_syntax = false,

            -- rust-analyzer may be long before it has finished to compute
            -- Before rust-analyzer provides code semantic use this syntax file 
            temporary_highlight_syntax = package_root.."syntax/simple_rust.vim",
          },

        -- automatically set inlay hints (type hints)
        -- There is an issue due to which the hints are not applied on the first
        -- opened file. For now, write to the file to trigger a reapplication of
        -- the hints or just run :RustSetInlayHints.
        -- default: true
        autoSetHints = true,

        -- automatically set code lenses 
        -- There is an issue. For now, write to the file to trigger a reapplication of
        -- the code lenses or just run :RustSetCodeLens.
        -- default: true
        autoSetCodeLenses = true,

        -- whether to show hover actions inside the hover window
        -- this overrides the default hover handler so something like lspsaga.nvim's hover would be overriden by this
        -- default: true
        hover_with_actions = true,

        -- These apply to the default RustRunnables command
        runnables = {
            -- whether to use telescope for selection menu or not
            -- default: true
            use_telescope = true

            -- rest of the opts are forwarded to telescope
        },

        -- These apply to the default RustSetInlayHints command
        inlay_hints = {

            -- Only show inlay hints for the current line
            only_current_line = false,

            -- Event which triggers a refersh of the inlay hints.
            -- You can make this "CursorMoved" or "CursorMoved,CursorMovedI" but
            -- not that this may cause  higher CPU usage.
            -- This option is only respected when only_current_line and
            -- autoSetHints both are true.
            only_current_line_autocmd = "CursorHold",

            -- wheter to show parameter hints with the inlay hints or not
            -- default: true
            show_parameter_hints = true,

            -- prefix for parameter hints
            -- default: "<-"
            parameter_hints_prefix = "    ",

            -- prefix for all the other hints (type, chaining)
            -- default: "=>"
            other_hints_prefix = "   :",

            -- whether to align to the lenght of the longest line in the file
            max_len_align = false,

            -- padding from the left if max_len_align is true
            max_len_align_padding = 1,

            -- whether to align to the extreme right or not
            right_align = false,

            -- padding from the right if right_align is true
            right_align_padding = 7,

            -- The color of the hints
            highlight = "InlayHints"
        },

        hover_actions = {
            -- the border that is used for the hover window
            -- see vim.api.nvim_open_win()
            border = {
                {"╭", "FloatBorder"}, {"─", "FloatBorder"},
                {"╮", "FloatBorder"}, {"│", "FloatBorder"},
                {"╯", "FloatBorder"}, {"─", "FloatBorder"},
                {"╰", "FloatBorder"}, {"│", "FloatBorder"}
            },

            -- whether the hover action window gets automatically focused
            -- default: false
            auto_focus = false,

            -- weither code_actions appear in floating window (true) or
            -- in a dedicated window
            float_code_actions = false,

            -- weither hover appear in floating window
            float_hover = true
        },

        -- debugger support configuration
        dap_adapter = {
          type = 'executable',
          command = 'lldb-vscode',
          name = "rt_lldb"
        }

    },

    -- all the opts to send to nvim-lspconfig
    -- these override the defaults set by i-love-rust.nvim
    -- see https://github.com/neovim/nvim-lspconfig/blob/master/CONFIG.md#rust_analyzer
    -- rust-analyer options
	  server = {
	  	settings = {
	  		["rust-analyzer"] ={
	  			hoverActions = {
	  				references=true,
	  				},
	  			lens = {
	  				methodReferences = true,
	  				},
	 			}
	 		}
	 	}
}
require 'i-love-rust'.setup(opts)
EOF
```

# Functionalities

## code actions
By default code action will appear in a side window and are updated when the
cursor moved. Supposing the mapping to trigger a code action is `" a"`:
  - Hitting `" a"` in normal or visual mode will show (if it does not already
    appear as a side bar) code action as a numbered list of item. What appear
    in visual mode may differ from what appear in normal mode.
  - Hitting `"<count> a"`, (for exemple `"3 a"`) will trigger the code action
    numbered `<count>`.. 

You can alternate between floating/side bar window with the command
`:RustSwitchCodeActionsStyle`

![FloatingCodeActions](https://gitlab.com/api/v4/projects/okannen%2Fdoc_pictures/repository/files/i-love-rust.nvim%2Ffloating_action.png/raw?ref=master)

Supplementary code actions may appear when selecting a range in visual mode.
The "Sort fields alphabeticaly" code action only appear when visualy selecting fields
of a struct.
![range_code_action](https://gitlab.com/api/v4/projects/okannen%2Fdoc_pictures/repository/files/i-love-rust.nvim%2Frange_code_action.png/raw?ref=master)


## hover 
This show either a documentation of the item bellow the cursor or in visual
mode, the type of the selected expression.

Exemple of hover in normal mode:


By default hover appear in a floating window, but the tool can be configured to
always show the hover content in a side window. You can alternate between
floating/side bar window with the command `:RustSwitchHoverStyle`

Hitting the keymap associated to `"hover"` will open the floating window if not
configured as a side bar.  Hitting the keymap preceded by a count (for example
`"3 h"`) will trigger the action that is shown in the hover.

## quick fix 

By hitting the `"quick fix"` (`" qf"`) the error bellow the cursor
may be directly fixed. If rust-analyzer propose more than one quick fix, a
hover will appear to select the right one. The type `"<count> qf"` to apply
your choice.

## run 

Hitting the keymap associated to `"run"` (`" ru"`) will run the test or
main function that contain the cursor or within a test module, all tests of
this module.

You can also pass argument to the main function using the command
`:RustRunSingle <args>`.

## debug 

Hitting the keymap associated to `"debug"` (`" rd"`) will launch the
debugger on the test or main function that contain the cursor.  Before
launching the debugger add a breakpoint (keymap `" b"`) otherwise the debugger
may finish before you have the time to notice it has started!

You can also pass argument to the main function using the command
`:RustDebugSingle <args>`.

## automatic minimal signature help

During insertion or code navigation in normal mode, when the cursor is the opening parenthesis of a function
call the signature of the function called is shown in a hover then immediatly closed when the cursor move.

When on a comma, the name of the paramter following the comma will also appear.


## list unsafe

Hitting the keymap associated to `"list unsafe"` (`" qu"`) or `"list workspace unsafe"` (`" qU"`) will put
in the location list the location of all unsafe function calls, unsafe function declarations, unsafe trait declarations and
unsafe trait implementations that are found either in the current package or in the entire workspace.

This functionality is intended to help the review of unsafe code: 
  - to ensure a `SAFETY` documentation at location of all unsafe functions calls and trait
  implementation that justify that the unsafe code actualy follow the documented requirements.
  - to ensure that a `# Safety` doc paragraph exist for all unsafe delcarations (which is linted by the compiler)

If the workspace or the package is large, the creation of the list may take a significative time: expect 1s per source file.

![list_unsafe](https://gitlab.com/api/v4/projects/okannen%2Fdoc_pictures/repository/files/i-love-rust.nvim%2Flist_unsafe.png/raw?ref=master)

## Semantic highlighting

By default, `i-love-rust` will remove default psychedelic syntax highlighting
by an highlighting based on rust-analyzer LSP semantic token support. This can be customized or
be disabled by setting the options `tools.highlight=false`.

This is realy cool because, thanks to the LSP server we can highlight
specificaly mutable variables, unsafe function call etc.. The space of
possiblity is HUGE and I hope you will push your prefered highlighting function
to this repo.

The default definition is given bellow: 
```vim 
highlight = {
    -- which function for highliting, default to require"i-love-rust".semantic.simple_highlighter
    --  param: token
    --    token.type => string, token type
    --    token.modifiers => list of token modifiers
    --    token.modifiers:contains(modifier) => return true if modifier in modifiers list
    --    token:highlight(group) => apply higlight group to token
    --    token:get_text() => return the token string
    --  exemple => see `simple_highlighter` in module "i-love-rust/semantic" 
    --  to see list of types and modifiers, run the command
    --    :lua print(vim.inspect(require"i-love-rust.semantic".legend))
    token_highlighter = nil,

    -- shall vim syntax be kept
    overlay_syntax = false,

    -- rust-analyzer may be long, before it has finish to compute
    -- code semantic use this syntax file
    temporary_highlight_syntax = package_root.."syntax/simple_rust.vim",
  },
```
The default highlighting function is declared in
`<plugindir>/lua/i-love-rust/highlighting.lua`.  What it does:
  - bold all item declaration, and underline them if they are public
  - italisize mutable variables and method call whose receiver is `&mut self`
  - color in orange every unsafe function/method declaration and *calls* and
  every unsafe trait declaration or implementation
  - when a variable argument is consumed, turn it into italic
  - highlight test docs but with fadded colors
  - color traits in blue to differantiate them from types
  - color literals
  - color macros and attribute
  - does not use more colors for the rest to avoid color overdose but you
  can add as much color as you want.

Bellow is the definition of the default token highlighter
```lua
function M.simple_highlighter(token)
  
  -- First we decide how to set the color

  if token:is_a("comment") then
    token:highlight("Grey")

  -- use faded color for doc tests
  elseif token:has_modifier("documentation") or token:has_modifier("injected") then
    if token:has_modifier("attribute") then
      token:highlight("AzureB3")
    elseif token:is_a("number") then
      token:highlight("MagentaB2")
    elseif token:is_a("boolean") then
      token:highlight("MagentaB2")
    elseif token:is_a("string") then
      token:highlight("GreenB2")
    elseif token:is_a("character") then
      token:highlight("ChartreuseGreenB2")
    elseif token:is_a("macro") then
      token:highlight("CyanB2")
    elseif token:is_a("interface") then
      token:highlight("BlueB2")
    elseif token:has_modifier("unsafe") then
      token:highlight("SalmonB2")
    else
      token:highlight("Grey")
    end

  else

    -- use normal color for code
    if token:has_modifier("attribute") then
      token:highlight("OrangeB2")
    elseif token:is_a("number") then
      token:highlight("MagentaF1")
    elseif token:is_a("boolean") then
      token:highlight("MagentaF1")
    elseif token:is_a("string") then
      token:highlight("GreenF1")
    elseif token:is_a("character") then
      token:highlight("ChartreuseGreenF1")
    elseif token:is_a("macro") then
      token:highlight("CyanF1")
    elseif token:is_a("interface") then
      token:highlight("BlueF1")
    elseif token:has_modifier("unsafe") then
      token:highlight("Salmon")
    end
  end

  -- Lastly we define if we bold/italisize/underline
  -- For that to works you need a font that support bold
  -- and italic

  if token:is_a("keyword") then
	  if token:get_text() == "impl" then
      token:highlight("Bold")
    elseif token:has_modifier("controlFlow") then
      token:highlight("Italic")
    end
    return
  end

  if token:has_modifier("declaration") then
    if token:is_a("selfKeyword") or token:is_a("parameter") or token:is_a("typeParameter")
      or token:is_a("variable") or token:is_a("lifetime") then
      token:highlight("Bold")
      if token:has_modifier("documentation") or token:has_modifier("injected") then
        token:highlight("GreyB1")
      else
        token:highlight("GreyF3")
      end
    else
      token:highlight("Bold")
    end
    if token:has_modifier("public") then
      token:highlight("Underline")
    end
  elseif token:has_modifier("controlFlow") then
    token:highlight("Italic")
  elseif token:has_modifier("mutable") then
    token:highlight("Italic")
  elseif token:has_modifier("consuming") then
    token:highlight("Italic")
  end

end
```
The code should be self explanatory. Notice the following:
  - the parameter of the method `token:highlight` is the name of an `higlight
    group`. The ones used in this function are defined in the function
    `setup_higroups`. There are color higlight groups:
    "Red","Salmon","Orange","Yellow","ChartreuseGreen","Green","SpringGreen","Cyan","Azure","Blue","Violet","Magenta","Rose","Grey"
    all associated to nuances, from closer to foreground color to closer to
    background colors "F5", "F4", "F3", "F2", "F1", "", "B1", "B2","B3", "B4",
    B5". For exemple, if the background is dark and the forground is light,
    "RedF5" is lighter than "Red", which is lighter than "RedB1". All those
    color groups can be mixed with style groups "Italic", "Bold", "Underline",
    "StrikeThrough", "Inverse", "Reverse". Those style may be mixed together
    with some limits imposed by the installed font ("Bold"+"Italic" is not
    often supported)
  - the list of token types and modifiers can be view by running `:lua
    print(vim.inspect(require"i-love-rust.semantic".legend))` in the command
    line. The semantic associated to the token under the cursor can be viewed
    by running "token semantic" (hit " y" in normal mode)
  - if your color scheme provides those colors they will be used instead (for exemple
    the plugin gruvbox-material provides those colors).

You can define your own semantic highlighting. For example with the folowing
setting, the classical syntax highlighting is not turned off but every unsafe
function/method/operator call/declaration and the `unsafe` keyword are turned
into red and every thing that is mutable into italic:

```lua
highlight = {
    overlay_syntax = true,
    token_highlighter = function(token)
      if token:has_modifier("unsafe") then
        token:highlight("Red")
      end
      if token:has_modifier("mutable") then
        token:highlight("Italic")
      end
    end
  },
```


## declaration, definition
Hitting the keymap associated to `"declaration"` will cause a jump of the
cursor to the declaration of the entity bellow the current cursor position

## line diagnostics
Hitting the keymap associated to `"line diagnostics"` will show a hover with
current line diagnostic details

## open external doc
Hitting the keymap associated to `"open external doc"` (`" H"`) will open the
browser and show-up the documentation in `docs.rs` site.

## incoming call
Hitting the keymap associated to `"incoming calls"` (`" i"`) will show a list
of all call site of the method or function bellow the cursor

## outgoing call
Hitting the keymap associated to `"outgoing calls"` (`" o"`) will show a list
of all function called within the body of the current function or method.

## join line 
Hitting the keymap associated to `"join line"` (`" J"`) will join to
consecutive line smartly.

## implementation
Jumps to the implementation of the entity bellow the cursor

## parent module
Jumps to the parent module of the current module

## Todo: document all functionality in keymaps and commands 

## Other command facilities

`:RustExpanMacro` output macro expansion of the macro under the cursor

`:RustCrageGraph` will show crate graph dependency 

`:RustCrageGraph full` will a full show crate graph dependency 

`:RustReloadWorkspace` => guess.

`:RustOpenCargo` open Cargo.toml file

**Those two last one are realy cool**: they are like a `:s` that understand code structure
`:RustSearchReplace <SSR>` apply the [structural search an replace](https://rust-analyzer.github.io/manual.html#structural-search-and-replace) <SSR> to the range or the current line (use it as :s)

`:RustWorkspaceSearchReplace <SSR>` apply the [structural search an replace](https://rust-analyzer.github.io/manual.html#structural-search-and-replace) <SSR> to the entire workspace

# Credit

This pluggin, was initialy a fork of rust-tools.nvim. Since then it as taken its own way.
