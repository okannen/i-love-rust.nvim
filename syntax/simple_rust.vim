" copie of github.com/rust-lang/rust.vim/syntax/rust.vim
"
" Syntax definitions {{{1
" Basic keywords {{{2
syn keyword   simplerustConditional match if else
syn keyword   simplerustRepeat loop while
" `:syn match` must be used to prioritize highlighting `for` keyword.
syn match     simplerustRepeat /\<for\>/
" Highlight `for` keyword in `impl ... for ... {}` statement. This line must
" be put after previous `syn match` line to overwrite it.
syn match     simplerustKeyword /\%(\<impl\>.\+\)\@<=\<for\>/
syn keyword   simplerustRepeat in
syn keyword   simplerustTypedef type nextgroup=simplerustIdentifier skipwhite skipempty
syn keyword   simplerustStructure struct enum nextgroup=simplerustIdentifier skipwhite skipempty
syn keyword   simplerustUnion union nextgroup=simplerustIdentifier skipwhite skipempty contained
syn match simplerustUnionContextual /\<union\_s\+\%([^[:cntrl:][:space:][:punct:][:digit:]]\|_\)\%([^[:cntrl:][:punct:][:space:]]\|_\)*/ transparent contains=simplerustUnion
syn keyword   simplerustOperator    as
syn keyword   simplerustExistential existential nextgroup=simplerustTypedef skipwhite skipempty contained
syn match simplerustExistentialContextual /\<existential\_s\+type/ transparent contains=simplerustExistential,simplerustTypedef

syn match     simplerustAssert      "\<assert\(\w\)*!" contained
syn match     simplerustPanic       "\<panic\(\w\)*!" contained
syn match     simplerustAsync       "\<async\%(\s\|\n\)\@="
syn keyword   simplerustKeyword     break
syn keyword   simplerustKeyword     box
syn keyword   simplerustKeyword     continue
syn keyword   simplerustKeyword     crate
syn keyword   simplerustKeyword     extern nextgroup=simplerustExternCrate,simplerustObsoleteExternMod skipwhite skipempty
syn keyword   simplerustKeyword     fn nextgroup=simplerustFuncName skipwhite skipempty
syn keyword   simplerustImplKeyword impl
syn keyword   simplerustKeyword     let
syn keyword   simplerustKeyword     macro
syn keyword   simplerustKeyword     pub nextgroup=simplerustPubScope skipwhite skipempty
syn keyword   simplerustKeyword     return
syn keyword   simplerustKeyword     yield
syn keyword   simplerustSuper       super
syn keyword   simplerustKeyword     where
syn keyword   simplerustUnsafeKeyword unsafe
syn keyword   simplerustKeyword     use nextgroup=simplerustModPath skipwhite skipempty
" FIXME: Scoped impl's name is also fallen in this category
syn keyword   simplerustKeyword     mod trait nextgroup=simplerustIdentifier skipwhite skipempty
syn keyword   simplerustStorage     move mut ref static const
syn match     simplerustDefault     /\<default\ze\_s\+\(impl\|fn\|type\|const\)\>/
syn keyword   simplerustAwait       await
syn match     simplerustKeyword     /\<try\>!\@!/ display

syn keyword simplerustPubScopeCrate crate contained
syn match simplerustPubScopeDelim /[()]/ contained
syn match simplerustPubScope /([^()]*)/ contained contains=simplerustPubScopeDelim,simplerustPubScopeCrate,simplerustSuper,simplerustModPath,simplerustModPathSep,simplerustSelf transparent

syn keyword   simplerustExternCrate crate contained nextgroup=simplerustIdentifier,simplerustExternCrateString skipwhite skipempty
" This is to get the `bar` part of `extern crate "foo" as bar;` highlighting.
syn match   simplerustExternCrateString /".*"\_s*as/ contained nextgroup=simplerustIdentifier skipwhite transparent skipempty contains=simplerustString,simplerustOperator
syn keyword   simplerustObsoleteExternMod mod contained nextgroup=simplerustIdentifier skipwhite skipempty

syn match     simplerustIdentifier  contains=simplerustIdentifierPrime "\%([^[:cntrl:][:space:][:punct:][:digit:]]\|_\)\%([^[:cntrl:][:punct:][:space:]]\|_\)*" display contained
syn match     simplerustFuncName    "\%(r#\)\=\%([^[:cntrl:][:space:][:punct:][:digit:]]\|_\)\%([^[:cntrl:][:punct:][:space:]]\|_\)*" display contained

syn region simplerustMacroRepeat matchgroup=simplerustMacroRepeatDelimiters start="$(" end="),\=[*+]" contains=TOP
syn match simplerustMacroVariable "$\w\+"
syn match simplerustRawIdent "\<r#\h\w*" contains=NONE

" Reserved (but not yet used) keywords {{{2
syn keyword   simplerustReservedKeyword become do priv typeof unsized abstract virtual final override

" Built-in types {{{2
syn keyword   simplerustType        isize usize char bool u8 u16 u32 u64 u128 f32
syn keyword   simplerustType        f64 i8 i16 i32 i64 i128 str Self

" Things from the libstd v1 prelude (src/libstd/prelude/v1.rs) {{{2
" This section is just straight transformation of the contents of the prelude,
" to make it easy to update.

" Reexported core operators {{{3
syn keyword   simplerustTrait       Copy Send Sized Sync
syn keyword   simplerustTrait       Drop Fn FnMut FnOnce

" Reexported functions {{{3
" There’s no point in highlighting these; when one writes drop( or drop::< it
" gets the same highlighting anyway, and if someone writes `let drop = …;` we
" don’t really want *that* drop to be highlighted.
"syn keyword simplerustFunction drop

" Reexported types and traits {{{3
syn keyword simplerustTrait Box
syn keyword simplerustTrait ToOwned
syn keyword simplerustTrait Clone
syn keyword simplerustTrait PartialEq PartialOrd Eq Ord
syn keyword simplerustTrait AsRef AsMut Into From
syn keyword simplerustTrait Default
syn keyword simplerustTrait Iterator Extend IntoIterator
syn keyword simplerustTrait DoubleEndedIterator ExactSizeIterator
syn keyword simplerustEnum Option
syn keyword simplerustEnumVariant Some None
syn keyword simplerustEnum Result
syn keyword simplerustEnumVariant Ok Err
syn keyword simplerustTrait SliceConcatExt
syn keyword simplerustTrait String ToString
syn keyword simplerustTrait Vec

" Other syntax {{{2
syn keyword   simplerustSelf        self
syn keyword   simplerustBoolean     true false

" If foo::bar changes to foo.bar, change this ("::" to "\.").
" If foo::bar changes to Foo::bar, change this (first "\w" to "\u").
syn match     simplerustModPath     "\w\(\w\)*::[^<]"he=e-3,me=e-3
syn match     simplerustModPathSep  "::"

syn match     simplerustFuncCall    "\w\(\w\)*("he=e-1,me=e-1
syn match     simplerustFuncCall    "\w\(\w\)*::<"he=e-3,me=e-3 " foo::<T>();

" This is merely a convention; note also the use of [A-Z], restricting it to
" latin identifiers rather than the full Unicode uppercase. I have not used
" [:upper:] as it depends upon 'noignorecase'
"syn match     simplerustCapsIdent    display "[A-Z]\w\(\w\)*"

syn match     simplerustOperator     display "\%(+\|-\|/\|*\|=\|\^\|&\||\|!\|>\|<\|%\)=\?"
" This one isn't *quite* right, as we could have binary-& with a reference
syn match     simplerustSigil        display /&\s\+[&~@*][^)= \t\r\n]/he=e-1,me=e-1
syn match     simplerustSigil        display /[&~@*][^)= \t\r\n]/he=e-1,me=e-1
" This isn't actually correct; a closure with no arguments can be `|| { }`.
" Last, because the & in && isn't a sigil
syn match     simplerustOperator     display "&&\|||"
" This is simplerustArrowCharacter rather than simplerustArrow for the sake of matchparen,
" so it skips the ->; see http://stackoverflow.com/a/30309949 for details.
syn match     simplerustArrowCharacter display "->"
syn match     simplerustQuestionMark display "?\([a-zA-Z]\+\)\@!"

syn match     simplerustMacro       '\w\(\w\)*!' contains=simplerustAssert,simplerustPanic
syn match     simplerustMacro       '#\w\(\w\)*' contains=simplerustAssert,simplerustPanic

syn match     simplerustEscapeError   display contained /\\./
syn match     simplerustEscape        display contained /\\\([nrt0\\'"]\|x\x\{2}\)/
syn match     simplerustEscapeUnicode display contained /\\u{\%(\x_*\)\{1,6}}/
syn match     simplerustStringContinuation display contained /\\\n\s*/
syn region    simplerustString      matchgroup=simplerustStringDelimiter start=+b"+ skip=+\\\\\|\\"+ end=+"+ contains=simplerustEscape,simplerustEscapeError,simplerustStringContinuation
syn region    simplerustString      matchgroup=simplerustStringDelimiter start=+"+ skip=+\\\\\|\\"+ end=+"+ contains=simplerustEscape,simplerustEscapeUnicode,simplerustEscapeError,simplerustStringContinuation,@Spell
syn region    simplerustString      matchgroup=simplerustStringDelimiter start='b\?r\z(#*\)"' end='"\z1' contains=@Spell

" Match attributes with either arbitrary syntax or special highlighting for
" derives. We still highlight strings and comments inside of the attribute.
syn region    simplerustAttribute   start="#!\?\[" end="\]" contains=@simplerustAttributeContents,simplerustAttributeParenthesizedParens,simplerustAttributeParenthesizedCurly,simplerustAttributeParenthesizedBrackets,simplerustDerive
syn region    simplerustAttributeParenthesizedParens matchgroup=simplerustAttribute start="\w\%(\w\)*("rs=e end=")"re=s transparent contained contains=simplerustAttributeBalancedParens,@simplerustAttributeContents
syn region    simplerustAttributeParenthesizedCurly matchgroup=simplerustAttribute start="\w\%(\w\)*{"rs=e end="}"re=s transparent contained contains=simplerustAttributeBalancedCurly,@simplerustAttributeContents
syn region    simplerustAttributeParenthesizedBrackets matchgroup=simplerustAttribute start="\w\%(\w\)*\["rs=e end="\]"re=s transparent contained contains=simplerustAttributeBalancedBrackets,@simplerustAttributeContents
syn region    simplerustAttributeBalancedParens matchgroup=simplerustAttribute start="("rs=e end=")"re=s transparent contained contains=simplerustAttributeBalancedParens,@simplerustAttributeContents
syn region    simplerustAttributeBalancedCurly matchgroup=simplerustAttribute start="{"rs=e end="}"re=s transparent contained contains=simplerustAttributeBalancedCurly,@simplerustAttributeContents
syn region    simplerustAttributeBalancedBrackets matchgroup=simplerustAttribute start="\["rs=e end="\]"re=s transparent contained contains=simplerustAttributeBalancedBrackets,@simplerustAttributeContents
syn cluster   simplerustAttributeContents contains=simplerustString,simplerustCommentLine,simplerustCommentBlock,simplerustCommentLineDocError,simplerustCommentBlockDocError
syn region    simplerustDerive      start="derive(" end=")" contained contains=simplerustDeriveTrait
" This list comes from src/libsyntax/ext/deriving/mod.rs
" Some are deprecated (Encodable, Decodable) or to be removed after a new snapshot (Show).
syn keyword   simplerustDeriveTrait contained Clone Hash RustcEncodable RustcDecodable Encodable Decodable PartialEq Eq PartialOrd Ord Rand Show Debug Default FromPrimitive Send Sync Copy

" dyn keyword: It's only a keyword when used inside a type expression, so
" we make effort here to highlight it only when Rust identifiers follow it
" (not minding the case of pre-2018 Rust where a path starting with :: can
" follow).
"
" This is so that uses of dyn variable names such as in 'let &dyn = &2'
" and 'let dyn = 2' will not get highlighted as a keyword.
syn match     simplerustKeyword "\<dyn\ze\_s\+\%([^[:cntrl:][:space:][:punct:][:digit:]]\|_\)" contains=simplerustDynKeyword
syn keyword   simplerustDynKeyword  dyn contained

" Number literals
syn match     simplerustDecNumber   display "\<[0-9][0-9_]*\%([iu]\%(size\|8\|16\|32\|64\|128\)\)\="
syn match     simplerustHexNumber   display "\<0x[a-fA-F0-9_]\+\%([iu]\%(size\|8\|16\|32\|64\|128\)\)\="
syn match     simplerustOctNumber   display "\<0o[0-7_]\+\%([iu]\%(size\|8\|16\|32\|64\|128\)\)\="
syn match     simplerustBinNumber   display "\<0b[01_]\+\%([iu]\%(size\|8\|16\|32\|64\|128\)\)\="

" Special case for numbers of the form "1." which are float literals, unless followed by
" an identifier, which makes them integer literals with a method call or field access,
" or by another ".", which makes them integer literals followed by the ".." token.
" (This must go first so the others take precedence.)
syn match     simplerustFloat       display "\<[0-9][0-9_]*\.\%([^[:cntrl:][:space:][:punct:][:digit:]]\|_\|\.\)\@!"
" To mark a number as a normal float, it must have at least one of the three things integral values don't have:
" a decimal point and more numbers; an exponent; and a type suffix.
syn match     simplerustFloat       display "\<[0-9][0-9_]*\%(\.[0-9][0-9_]*\)\%([eE][+-]\=[0-9_]\+\)\=\(f32\|f64\)\="
syn match     simplerustFloat       display "\<[0-9][0-9_]*\%(\.[0-9][0-9_]*\)\=\%([eE][+-]\=[0-9_]\+\)\(f32\|f64\)\="
syn match     simplerustFloat       display "\<[0-9][0-9_]*\%(\.[0-9][0-9_]*\)\=\%([eE][+-]\=[0-9_]\+\)\=\(f32\|f64\)"

" For the benefit of delimitMate
syn region simplerustLifetimeCandidate display start=/&'\%(\([^'\\]\|\\\(['nrt0\\\"]\|x\x\{2}\|u{\%(\x_*\)\{1,6}}\)\)'\)\@!/ end=/[[:cntrl:][:space:][:punct:]]\@=\|$/ contains=simplerustSigil,simplerustLifetime
syn region simplerustGenericRegion display start=/<\%('\|[^[:cntrl:][:space:][:punct:]]\)\@=')\S\@=/ end=/>/ contains=simplerustGenericLifetimeCandidate
syn region simplerustGenericLifetimeCandidate display start=/\%(<\|,\s*\)\@<='/ end=/[[:cntrl:][:space:][:punct:]]\@=\|$/ contains=simplerustSigil,simplerustLifetime

"simplerustLifetime must appear before simplerustCharacter, or chars will get the lifetime highlighting
syn match     simplerustLifetime    display "\'\%([^[:cntrl:][:space:][:punct:][:digit:]]\|_\)\%([^[:cntrl:][:punct:][:space:]]\|_\)*"
syn match     simplerustLabel       display "\'\%([^[:cntrl:][:space:][:punct:][:digit:]]\|_\)\%([^[:cntrl:][:punct:][:space:]]\|_\)*:"
syn match     simplerustLabel       display "\%(\<\%(break\|continue\)\s*\)\@<=\'\%([^[:cntrl:][:space:][:punct:][:digit:]]\|_\)\%([^[:cntrl:][:punct:][:space:]]\|_\)*"
syn match   simplerustCharacterInvalid   display contained /b\?'\zs[\n\r\t']\ze'/
" The groups negated here add up to 0-255 but nothing else (they do not seem to go beyond ASCII).
syn match   simplerustCharacterInvalidUnicode   display contained /b'\zs[^[:cntrl:][:graph:][:alnum:][:space:]]\ze'/
syn match   simplerustCharacter   /b'\([^\\]\|\\\(.\|x\x\{2}\)\)'/ contains=simplerustEscape,simplerustEscapeError,simplerustCharacterInvalid,simplerustCharacterInvalidUnicode
syn match   simplerustCharacter   /'\([^\\]\|\\\(.\|x\x\{2}\|u{\%(\x_*\)\{1,6}}\)\)'/ contains=simplerustEscape,simplerustEscapeUnicode,simplerustEscapeError,simplerustCharacterInvalid

syn match simplerustShebang /\%^#![^[].*/
syn region simplerustCommentLine                                                  start="//"                      end="$"   contains=simplerustTodo,@Spell
syn region simplerustCommentLineDoc                                               start="//\%(//\@!\|!\)"         end="$"   contains=simplerustTodo,@Spell
syn region simplerustCommentLineDocError                                          start="//\%(//\@!\|!\)"         end="$"   contains=simplerustTodo,@Spell contained
syn region simplerustCommentBlock             matchgroup=simplerustCommentBlock         start="/\*\%(!\|\*[*/]\@!\)\@!" end="\*/" contains=simplerustTodo,simplerustCommentBlockNest,@Spell
syn region simplerustCommentBlockDoc          matchgroup=simplerustCommentBlockDoc      start="/\*\%(!\|\*[*/]\@!\)"    end="\*/" contains=simplerustTodo,simplerustCommentBlockDocNest,simplerustCommentBlockDocRustCode,@Spell
syn region simplerustCommentBlockDocError     matchgroup=simplerustCommentBlockDocError start="/\*\%(!\|\*[*/]\@!\)"    end="\*/" contains=simplerustTodo,simplerustCommentBlockDocNestError,@Spell contained
syn region simplerustCommentBlockNest         matchgroup=simplerustCommentBlock         start="/\*"                     end="\*/" contains=simplerustTodo,simplerustCommentBlockNest,@Spell contained transparent
syn region simplerustCommentBlockDocNest      matchgroup=simplerustCommentBlockDoc      start="/\*"                     end="\*/" contains=simplerustTodo,simplerustCommentBlockDocNest,@Spell contained transparent
syn region simplerustCommentBlockDocNestError matchgroup=simplerustCommentBlockDocError start="/\*"                     end="\*/" contains=simplerustTodo,simplerustCommentBlockDocNestError,@Spell contained transparent

" FIXME: this is a really ugly and not fully correct implementation. Most
" importantly, a case like ``/* */*`` should have the final ``*`` not being in
" a comment, but in practice at present it leaves comments open two levels
" deep. But as long as you stay away from that particular case, I *believe*
" the highlighting is correct. Due to the way Vim's syntax engine works
" (greedy for start matches, unlike Rust's tokeniser which is searching for
" the earliest-starting match, start or end), I believe this cannot be solved.
" Oh you who would fix it, don't bother with things like duplicating the Block
" rules and putting ``\*\@<!`` at the start of them; it makes it worse, as
" then you must deal with cases like ``/*/**/*/``. And don't try making it
" worse with ``\%(/\@<!\*\)\@<!``, either...

syn keyword simplerustTodo contained TODO FIXME XXX NB NOTE SAFETY

" asm! macro {{{2
syn region simplerustAsmMacro matchgroup=simplerustMacro start="\<asm!\s*(" end=")" contains=simplerustAsmDirSpec,simplerustAsmSym,simplerustAsmConst,simplerustAsmOptionsGroup,simplerustComment.*,simplerustString.*

" Clobbered registers
syn keyword simplerustAsmDirSpec in out lateout inout inlateout contained nextgroup=simplerustAsmReg skipwhite skipempty
syn region  simplerustAsmReg start="(" end=")" contained contains=simplerustString

" Symbol operands
syn keyword simplerustAsmSym sym contained nextgroup=simplerustAsmSymPath skipwhite skipempty
syn region  simplerustAsmSymPath start="\S" end=",\|)"me=s-1 contained contains=simplerustComment.*,simplerustIdentifier

" Const
syn region  simplerustAsmConstBalancedParens start="("ms=s+1 end=")" contained contains=@simplerustAsmConstExpr
syn cluster simplerustAsmConstExpr contains=simplerustComment.*,simplerust.*Number,simplerustString,simplerustAsmConstBalancedParens
syn region  simplerustAsmConst start="const" end=",\|)"me=s-1 contained contains=simplerustStorage,@simplerustAsmConstExpr

" Options
syn region  simplerustAsmOptionsGroup start="options\s*(" end=")" contained contains=simplerustAsmOptions,simplerustAsmOptionsKey
syn keyword simplerustAsmOptionsKey options contained
syn keyword simplerustAsmOptions pure nomem readonly preserves_flags noreturn nostack att_syntax contained

" Folding rules {{{2
" Trivial folding rules to begin with.
" FIXME: use the AST to make really good folding
syn region simplerustFoldBraces start="{" end="}" transparent fold

if !exists("b:current_syntax_embed")
    let b:current_syntax_embed = 1
    syntax include @RustCodeInComment <sfile>:p:h/simple_rust.vim
    unlet b:current_syntax_embed

    " Currently regions marked as ```<some-other-syntax> will not get
    " highlighted at all. In the future, we can do as vim-markdown does and
    " highlight with the other syntax. But for now, let's make sure we find
    " the closing block marker, because the rules below won't catch it.
    syn region simplerustCommentLinesDocNonRustCode matchgroup=simplerustCommentDocCodeFence start='^\z(\s*//[!/]\s*```\).\+$' end='^\z1$' keepend contains=simplerustCommentLineDoc

    " We borrow the rules from simplerust’s src/librustdoc/html/markdown.rs, so that
    " we only highlight as Rust what it would perceive as Rust (almost; it’s
    " possible to trick it if you try hard, and indented code blocks aren’t
    " supported because Markdown is a menace to parse and only mad dogs and
    " Englishmen would try to handle that case correctly in this syntax file).
    syn region simplerustCommentLinesDocRustCode matchgroup=simplerustCommentDocCodeFence start='^\z(\s*//[!/]\s*```\)[^A-Za-z0-9_-]*\%(\%(should_panic\|no_run\|ignore\|allow_fail\|simplerust\|test_harness\|compile_fail\|E\d\{4}\|edition201[58]\)\%([^A-Za-z0-9_-]\+\|$\)\)*$' end='^\z1$' keepend contains=@RustCodeInComment,simplerustCommentLineDocLeader
    syn region simplerustCommentBlockDocRustCode matchgroup=simplerustCommentDocCodeFence start='^\z(\%(\s*\*\)\?\s*```\)[^A-Za-z0-9_-]*\%(\%(should_panic\|no_run\|ignore\|allow_fail\|simplerust\|test_harness\|compile_fail\|E\d\{4}\|edition201[58]\)\%([^A-Za-z0-9_-]\+\|$\)\)*$' end='^\z1$' keepend contains=@RustCodeInComment,simplerustCommentBlockDocStar
    " Strictly, this may or may not be correct; this code, for example, would
    " mishighlight:
    "
    "     /**
    "     ```simplerust
    "     println!("{}", 1
    "     * 1);
    "     ```
    "     */
    "
    " … but I don’t care. Balance of probability, and all that.
    syn match simplerustCommentBlockDocStar /^\s*\*\s\?/ contained
    syn match simplerustCommentLineDocLeader "^\s*//\%(//\@!\|!\)" contained
endif

" Default highlighting {{{1
hi def link simplerustDecNumber       simplerustNumber
hi def link simplerustHexNumber       simplerustNumber
hi def link simplerustOctNumber       simplerustNumber
hi def link simplerustBinNumber       simplerustNumber
hi def link simplerustTrait           simplerustType
hi def link simplerustDeriveTrait     simplerustDerive

hi def link simplerustMacroRepeatDelimiters   Normal
hi def link simplerustMacroVariable Normal
hi def link simplerustSigil         Normal
hi def link simplerustEscape        String
hi def link simplerustEscapeUnicode simplerustEscape
hi def link simplerustEscapeError   Error
hi def link simplerustStringContinuation String
hi def link simplerustString        String
hi def link simplerustStringDelimiter String
hi def link simplerustCharacterInvalid Error
hi def link simplerustCharacterInvalidUnicode simplerustCharacterInvalid
hi def link simplerustCharacter     Character
hi def link simplerustNumber        Number
hi def link simplerustBoolean       Boolean
hi def link simplerustEnum          Normal
hi def link simplerustEnumVariant   Normal
hi def link simplerustConstant      Normal
hi def link simplerustSelf          Normal
hi def link simplerustFloat         Float
hi def link simplerustArrowCharacter simplerustOperator
hi def link simplerustOperator      Normal
hi def link simplerustKeyword       Normal
hi def link simplerustDynKeyword    simplerustKeyword

hi def link simplerustTypedef       Normal " More precise is Typedef, but it doesn't feel right for Rust
hi def link simplerustStructure     Normal " More precise is Structure
hi def link simplerustUnion         simplerustStructure
hi def link simplerustExistential   simplerustKeyword
hi def link simplerustPubScopeDelim Normal
hi def link simplerustPubScopeCrate simplerustKeyword
hi def link simplerustSuper         simplerustKeyword
hi def link simplerustReservedKeyword Error
hi def link simplerustIdentifier    Normal
hi def link simplerustCapsIdent     simplerustIdentifier
hi def link simplerustModPath       Normal
hi def link simplerustModPathSep    Normal
hi def link simplerustFunction      Normal
hi def link simplerustFuncName      simplerustIdentifierPrime
hi def link simplerustFuncCall      Normal
hi def link simplerustShebang       Comment
hi def link simplerustCommentLine   Comment
hi def link simplerustCommentLineDoc SpecialComment
hi def link simplerustCommentLineDocLeader simplerustCommentLineDoc
hi def link simplerustCommentLineDocError Error
hi def link simplerustCommentBlock  simplerustCommentLine
hi def link simplerustCommentBlockDoc simplerustCommentLineDoc
hi def link simplerustCommentBlockDocStar simplerustCommentBlockDoc
hi def link simplerustCommentBlockDocError Error
hi def link simplerustCommentDocCodeFence simplerustCommentLineDoc
hi def link simplerustAssert        Macro
hi def link simplerustPanic         Macro
hi def link simplerustMacro         Macro
hi def link simplerustType          Normal
hi def link simplerustTodo          Macro
hi def link simplerustDefault       Normal
hi def link simplerustStorage       Normal
hi def link simplerustObsoleteStorage Error
hi def link simplerustLifetime      Normal
hi def link simplerustLabel         Normal
hi def link simplerustExternCrate   Normal
hi def link simplerustObsoleteExternMod Error
hi def link simplerustAsync         simplerustKeyword
hi def link simplerustAwait         simplerustKeyword
hi def link simplerustAsmDirSpec    simplerustKeyword
hi def link simplerustAsmSym        simplerustKeyword
hi def link simplerustAsmOptions    simplerustKeyword
hi def link simplerustAsmOptionsKey simplerustAttribute

hi simplerustUnsafeKeyword guifg=#eb956a
hi simplerustImlKeyword    gui=bold
hi simplerustRepeat        gui=italic
hi simplerustConditional   gui=italic
hi simplerustIdentifier gui=bold
hi simplerustIdentifierPrime gui=bold
hi simplerustAttribute     guifg=#796033
hi simplerustDerive        guifg=#796033
hi simplerustQuestionMark  gui=italic
